#include "cdevice.h"
#include "sdk_error.h"
#include "utils.h"

#include "device_finder.h"
#include "device_info.h"
#include "device_battery.h"
#include "device_signal.h"
#include "device_envelope.h"
#include "device_electrodes.h"
#include "device_respiration.h"
#include "device_orientation.h"
#include "device_stimulation.h"
#include "device_mems.h"
#include "device_ecg.h"
#include "device_eeg.h"

#include "stdio.h"



bool dev_connect(Device* device) {
	const int resultCode = device_connect(device);
	if (resultCode != SDK_NO_ERROR) {
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot connect to device: %s\n", errorMsg);
		return false;
	}
	return true;
}

int main() {
	while (true) {
		printf("Enter device serial number to start device search or \"q\" to exit.\n");

		char serialStr[32];
		int resCode = read_console_value(&serialStr);
		if (resCode == 1) {
			Device* device = find_device_uses_callback(serialStr);
			if (device != NULL) {
				if (dev_connect(device)) {
					DemoMode mode;
					resCode = read_demo_mode_number(&mode);
					if (resCode == 1) {
						switch (mode) {
						case DemoModeDeviceInfo:
						{
							device_info_mode(device);
							break;
						}
						case DemoModeDeviceBattery:
						{
							device_battery_mode(device);
							break;
						}
						case DemoModeDeviceElectrodes:
						{
							device_electrodes_mode(device);
							break;
						}
						case DemoModeDeviceSignal:
						{
							device_signal_mode(device);
							break;
						}
						case DemoModeDeviceEnvelope:
						{
							device_envelope_mode(device);
							break;
						}
						case DemoModeRespiration:
						{
							device_respiration_mode(device);
							break;
						}
						case DemoModeECG:
						{
							device_ecg_mode(device);
							break;
						}
						case DemoModeOrientation:
						{
							device_orientation_mode(device);
							break;
						}
						case DemoModeMEMS:
						{
							device_mems_mode(device);
							break;
						}
						case DemoModeEEGIndexes:
						{
							device_eeg_mode(device);
							break;
						}
						case DemoModeStimulation:
						{
							device_stimulation_mode(device);
							break;
						}
						default:
							break;
						}
					}
					device_disconnect(device);
					device_delete(device);
					if (resCode > 1) { // Exit
						break;
					}
				}
				else {
					device_delete(device);
				}
			}
		}
		else if (resCode == 0) { // continue
			continue;
		}
		else { // Exit
			break;
		}
	}
	return 0;
}