#include "device_info.h"
#include "sdk_error.h"
#include "cparams.h"

#include <stdio.h>
#include <string.h>

void channel_type_to_string(const ChannelType chType, char* buffer, size_t buffer_length) {
    char* data = NULL;
    switch (chType) {
        case ChannelTypeSignal:
        {
            data = "Signal";
            break;
        }
        case ChannelTypeBrainbitSync:
        {
            data = "BrainbitSync";
            break;
        }
        case ChannelTypeBattery:
        {
            data = "Battery";
            break;
        }
        case ChannelTypeElectrodesState:
        {
            data = "ElectrodesState";
            break;
        }
        case ChannelTypeRespiration:
        {
            data = "Respiration";
            break;
        }
        case ChannelTypeMEMS:
        {
            data = "MEMS";
            break;
        }
        case ChannelTypeOrientation:
        {
            data = "Orientation";
            break;
        }
        case ChannelTypeResistance:
        {
            data = "Resistance";
            break;
        }
        case ChannelTypePedometer:
        {
            data = "Pedometer";
            break;
        }
        case ChannelTypeEnvelope:
        {
            data = "Envelope";
            break;
        }
        case ChannelTypeCustom:
        {
            data = "Custom";
            break;
        }
        default:
            data = "Unknown";
            break;
    }
    size_t len = strlen(data);
    memcpy(buffer, data, len * sizeof(char));
    buffer[len] = '\0';
}

void device_info_mode(Device* device) {
    printf("---------[Device info]-------------\n");

    printf("* Common params *\n");
    char buf[256];
    const size_t bufLen = sizeof(buf);

    if (device_read_Name(device, buf, bufLen) != SDK_NO_ERROR)
        return;
    printf("Name:          [%s]\n", buf);
    if (device_read_Address(device, buf, bufLen) != SDK_NO_ERROR)
        return;
    printf("Address:       [%s]\n", buf);
    if (device_read_SerialNumber(device, buf, bufLen) != SDK_NO_ERROR)
        return;
    printf("Serial number: [%s]\n", buf);
    FirmwareVersion fVersion;
    if (device_read_FirmwareVersion(device, &fVersion) != SDK_NO_ERROR)
        return;
    printf("Version:       [%d.%d]\n", fVersion.version, fVersion.build);
    FirmwareMode fMode;
    if (device_read_FirmwareMode(device, &fMode) != SDK_NO_ERROR)
        return;
    printf("Mode:          [%s]\n", fMode == FirmwareModeApplication ? "Application" : "Bootloader");


    ParamInfoArray params;
    int resultCode = device_available_parameters(device, &params);
    if (resultCode == SDK_NO_ERROR && params.info_count > 0) {
        printf("\n* Supported params *\n");
        bool hasError = false;
        for (size_t i = 0; i < params.info_count; ++i) {
            if (parameter_to_string(params.info_array[i].parameter, buf, bufLen) != SDK_NO_ERROR) {
                hasError = true;
                break;
            }
            printf("Parameter: [%20s] ", buf);
            if (parameter_access_to_string(params.info_array[i].access, buf, bufLen) != SDK_NO_ERROR) {
                hasError = true;
                break;
            }
            printf("Access: [%10s]\n", buf);
        }
        free_ParamInfoArray(params);
        if (hasError)
            return;
    }
    ChannelInfoArray channels;
    resultCode = device_available_channels(device, &channels);
    if (resultCode == SDK_NO_ERROR && channels.info_count > 0) {
        printf("\n* Supported device channels *\n");
        for (size_t i = 0; i < channels.info_count; ++i) {
            const ChannelInfo chInf = channels.info_array[i];
            channel_type_to_string(chInf.type, buf, bufLen);
            printf("Channel: [%20s] Type: [%16s] Index: [%lu]\n", chInf.name, buf, (uint32_t)chInf.index);
        }
        free_ChannelInfoArray(channels);
    }

    CommandArray commands;
    resultCode = device_available_commands(device, &commands);
    if (resultCode == SDK_NO_ERROR && commands.cmd_array_size > 0) {
        printf("\n* Supported commands *\n");
        for (size_t i = 0; i < commands.cmd_array_size; ++i) {
            const Command cmd = commands.cmd_array[i];
            command_to_string(commands.cmd_array[i], buf, bufLen);
            printf("Command: [%20s]\n", buf);
        }
        free_CommandArray(commands);
    }

    printf("-----------------------------------\n");
}