#include "device_finder.h"
#include "utils.h"
#include "cparams.h"
#include "sdk_error.h"

#include <stdio.h>
#include <stdbool.h>

typedef struct _EnumeratorResult {
    DeviceInfo deviceInfo;
    char* serial;
    bool founded;
} EnumeratorResult;

int filter_device(DeviceEnumerator* enumerator, const DeviceInfoArray* device_info_array, char* serial) {
    for (size_t i = 0; i < device_info_array->info_count; ++i)
    {
        Device* device = create_Device(enumerator, device_info_array->info_array[i]);
        if (device == NULL)
        {
            continue;
        }

        int resultCode = device_connect(device);
        if (resultCode != SDK_NO_ERROR)
        {
            device_delete(device);
            continue;
        }

        DeviceState deviceState;
        do
        {
            device_read_State(device, &deviceState);
        } while (deviceState != DeviceStateConnected);

        char deviceSerialNumber[32];
        resultCode = device_read_SerialNumber(device, deviceSerialNumber, 32);
        if (resultCode != SDK_NO_ERROR)
        {
            device_delete(device);
            continue;
        }
        
        if (strcmp(deviceSerialNumber, serial) == 0)
        {
            device_disconnect(device);
            device_delete(device);
            return i;
        }
    }
    return -1;
}

void enumerator_callback(DeviceEnumerator* enumerator, void* data) {
    DeviceInfoArray deviceInfoArray;
    const int resultCode = enumerator_get_device_list(enumerator, &deviceInfoArray);
    if (resultCode != SDK_NO_ERROR) {
        return;
    }
    EnumeratorResult* enResult = (EnumeratorResult*)data;
    int i = filter_device(enumerator, &deviceInfoArray, enResult->serial);
    if (i >= 0) {
        enResult->deviceInfo = deviceInfoArray.info_array[i];
        enResult->founded = true;
    }
    free_DeviceInfoArray(deviceInfoArray);
}

Device* find_device_uses_callback(char* serial) {
    DeviceEnumerator* enumerator = create_device_enumerator(DeviceTypeCallibri);
    if (enumerator == NULL) {
        char errorMsg[1024];
        sdk_last_error_msg(errorMsg, 1024);
        printf("Device enumerator is null: %s\n", errorMsg);
        return NULL;
    }
    DeviceListListener listener;
    EnumeratorResult enResult;
    enResult.serial = serial;
    enResult.founded = false;

    enumerator_set_device_list_changed_callback(enumerator, enumerator_callback, &listener, &enResult);

    int attempts = 10;
    bool founded = false;
    do {
        demo_sleep_ms(500);
        if (enResult.founded)
            break;
    } while (attempts-- > 0);
    Device* outDeviceInfo = NULL;
    enumerator_unsubscribe_device_list_changed(listener);

    if (enResult.founded) {
        printf("Device with serial number %s found. Name: %s, Address: %s\n", enResult.serial, enResult.deviceInfo.Name, enResult.deviceInfo.Address);
        outDeviceInfo = create_Device(enumerator, enResult.deviceInfo);
    } else {
        printf("Any devices with serial number %s found.\n", enResult.serial);
    }
    enumerator_delete(enumerator);
    return outDeviceInfo;
}