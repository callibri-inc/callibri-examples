#include "device_respiration.h"
#include "sdk_error.h"
#include "crespiration-channel.h"
#include "device_params_setter.h"
#include "cchannels.h"
#include "cparams.h"
#include "utils.h"

#include <stdio.h>

#define SIGNAL_SAMPLES_COUNT 20

typedef struct _ChannelData {
    double data[SIGNAL_SAMPLES_COUNT];
    size_t channelDataOffset;
    bool dataFilled;
} ChannelData;

void respiration_callback(AnyChannel* channel, size_t length, void* respirationDataOut) {
    ChannelData* data = (ChannelData*)respirationDataOut;
    if (!data->dataFilled && (length - data->channelDataOffset) >= SIGNAL_SAMPLES_COUNT) {
        size_t samples_read = 0;
        if (DoubleChannel_read_data((RespirationDoubleChannel*)channel, data->channelDataOffset, SIGNAL_SAMPLES_COUNT, data->data, SIGNAL_SAMPLES_COUNT, &samples_read) == SDK_NO_ERROR) {
            if (samples_read == SIGNAL_SAMPLES_COUNT) {
                data->channelDataOffset = data->channelDataOffset + samples_read;
                data->dataFilled = true;
            }
        }
    }
}

void device_respiration_mode(Device* device) {
    printf("-------[Respiration]------------\n");
    
    //Set parameters
    int resultCode = set_params_to_mode(device, DemoModeRespiration);
    if (resultCode != SDK_NO_ERROR) {
        printf("Something went wrong!");
        return;
    }
    //----------------------------------------

    RespirationDoubleChannel* respirationChannel = create_RespirationDoubleChannel(device);

    resultCode = device_execute(device, CommandStartRespiration);
    if (resultCode != SDK_NO_ERROR) {
        char errorMsg[1024];
        sdk_last_error_msg(errorMsg, 1024);
        printf("Cannot execute StartRespiration command: %s\n", errorMsg);
    } else {
        LengthListenerHandle respirationChannelListener = NULL;

        ChannelData respData;
        respData.channelDataOffset = 0;
        respData.dataFilled = false;

        AnyChannel_add_length_callback((AnyChannel*)respirationChannel, respiration_callback, &respirationChannelListener, &respData);

        int attempts = 5;
        do {
            while (!respData.dataFilled) {
                demo_sleep_ms(1);
            }

            printf("[%d respiration samples are received.]\n", SIGNAL_SAMPLES_COUNT);
            printf("     Sample:\n");
            for (size_t i = 0; i < SIGNAL_SAMPLES_COUNT; ++i) {
                printf("%.4f\n", respData.data[i]);                
            }
            respData.dataFilled = false;
        } while (attempts-- > 0);

        resultCode = device_execute(device, CommandStopRespiration);
        if (resultCode != SDK_NO_ERROR) {
            char errorMsg[1024];
            sdk_last_error_msg(errorMsg, 1024);
            printf("Cannot execute StopRespiration command: %s\n", errorMsg);
        }

        free_length_listener_handle(respirationChannelListener);
    }

    AnyChannel_delete((AnyChannel*)respirationChannel);
    printf("-----------------------------------\n");

}