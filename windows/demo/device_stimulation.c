#include "device_stimulation.h"
#include "sdk_error.h"
#include "cparams.h"
#include <stdio.h>
#include <string.h>

void device_stimulation_mode(Device* device) 
{
	printf("-----------[Stimulation]---------\n");

	ParamInfoArray availableParams;
	bool stimulationAvailable = false;
	int resultCode = device_available_parameters(device, &availableParams);
	for (size_t i = 0; i < availableParams.info_count; ++i)
	{
		stimulationAvailable = availableParams.info_array[i].parameter == ParameterStimulatorParamPack;
		if (stimulationAvailable)
		{
			break;
		}
	}
	if (stimulationAvailable)
	{
		printf("Stimulation available on this device!\n");
	}
	else 
	{
		printf("Stimulation not available on this device!\n");
		return;
	}
	

	StimulationParams currentParams;
	resultCode = device_read_StimulatorParamPack(device, &currentParams);
	if (resultCode != SDK_NO_ERROR) {
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Error while reading current stimulation params: %s\n", errorMsg);
		return;
	}

	printf("Current stimulation params: \n\tAmplitude: %d\n\tPulse width: %d\n\tFrequency: %d\n\tStimuls duration: %d\n", 
		currentParams.current, currentParams.pulse_width, currentParams.pulse_width, currentParams.stimulus_duration);

	printf("Set stimulation params to: Amplitude = 100, Pulse width = 120, Frequency = 60, Stimuls duration = 1\n");
	StimulationParams newParams;
	newParams.current = 100;
	newParams.frequency = 120;
	newParams.pulse_width = 60;
	newParams.stimulus_duration = 1;
	resultCode = device_set_StimulatorParamPack(device, newParams);
	if (resultCode != SDK_NO_ERROR) {
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Error while set stimulation params: %s\n", errorMsg);
		return;
	}

	resultCode = device_execute(device, CommandStartStimulation);
	if (resultCode != SDK_NO_ERROR) {
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot execute StartStimulation command: %s\n", errorMsg);
	}
	else
	{
		printf("Stimulation started...\n");
		demo_sleep_ms(3000);

		resultCode = device_execute(device, CommandStopStimulation);
		if (resultCode != SDK_NO_ERROR) {
			char errorMsg[1024];
			sdk_last_error_msg(errorMsg, 1024);
			printf("Cannot execute StopStimulation command: %s\n", errorMsg);
		}
		printf("Stimulation end!\n");
	}

}