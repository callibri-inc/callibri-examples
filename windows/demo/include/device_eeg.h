#ifndef DEVICE_EEG_H
#define DEVICE_EEG_H

#include "cdevice.h"

void device_eeg_mode(Device* device);

#endif // DEVICE_EEG_H