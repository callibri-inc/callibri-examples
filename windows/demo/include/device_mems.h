#ifndef DEVICE_MEMS_H
#define DEVICE_MEMS_H

#include "cdevice.h"

void device_mems_mode(Device* device);

#endif // DEVICE_MEMS_H
