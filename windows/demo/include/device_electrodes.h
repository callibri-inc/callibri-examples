#ifndef DEVICE_ELECTRODES_H
#define DEVICE_ELECTRODES_H

#include "cdevice.h"

void device_electrodes_mode(Device* device);

#endif // DEVICE_ELECTRODES_H