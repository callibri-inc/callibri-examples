#ifndef DEVICE_SIGNAL_H
#define DEVICE_SIGNAL_H

#include "cdevice.h"

void device_signal_mode(Device* device);

#endif // DEVICE_SIGNAL_H