#ifndef DEVICE_BATTERY_H
#define DEVICE_BATTERY_H

#include "cdevice.h"

void device_battery_mode(Device* device);

#endif // DEVICE_BATTERY_H