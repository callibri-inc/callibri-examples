#ifndef DEVICE_STIMULATION_H
#define DEVICE_STIMULATION_H

#include "cdevice.h"

void device_stimulation_mode(Device* device);

#endif // DEVICE_STIMULATION_H