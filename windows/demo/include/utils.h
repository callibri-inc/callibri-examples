#ifndef MAIN_UTILS_H
#define MAIN_UTILS_H

#include <stdint.h>

typedef enum _DemoMode {
    DemoModeUnknown,
    DemoModeDeviceInfo,
    DemoModeDeviceBattery,
    DemoModeDeviceElectrodes,
    DemoModeDeviceSignal,
    DemoModeDeviceEnvelope,
    DemoModeRespiration,
    DemoModeECG,
    DemoModeOrientation,
    DemoModeMEMS,
    DemoModeEEGIndexes,
    DemoModeStimulation
} DemoMode;

int read_demo_mode_number(DemoMode*);
int read_console_value(char*);
void demo_sleep_ms(uint32_t);

#endif // MAIN_UTILS_H