#ifndef DEVICE_ENVELOPE_H
#define DEVICE_ENVELOPE_H

#include "cdevice.h"

void device_envelope_mode(Device* device);

#endif // DEVICE_ENVELOPE_H