#ifndef DEVICE_PARAMS_SETTER_H
#define DEVICE_PARAMS_SETTER_H

#include "cdevice.h"
#include "utils.h"

int set_params_to_mode(Device* device, DemoMode mode);

#endif // DEVICE_PARAMS_SETTER_H