#ifndef DEVICE_ECG_H
#define DEVICE_ECG_H

#include "cdevice.h"

void device_ecg_mode(Device* device);

#endif // DEVICE_ECG_H