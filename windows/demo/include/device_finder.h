#ifndef DEVICE_FINDER_H
#define DEVICE_FINDER_H

#include "cdevice.h"

Device* find_device_uses_callback(char* serial);

#endif // DEVICE_FINDER_H