#ifndef DEVICE_ORIENTATION_H
#define DEVICE_ORIENTATION_H

#include "cdevice.h"

void device_orientation_mode(Device* device);

#endif // DEVICE_ORIENTATION_H