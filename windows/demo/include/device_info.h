#ifndef DEVICE_INFO_H
#define DEVICE_INFO_H

#include "cdevice.h"

void device_info_mode(Device* device);

#endif // DEVICE_INFO_H
