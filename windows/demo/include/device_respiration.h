#ifndef DEVICE_RESPIRATION_H
#define DEVICE_RESPIRATION_H

#include "cdevice.h"

void device_respiration_mode(Device* device);

#endif // DEVICE_RESPIRATION_H