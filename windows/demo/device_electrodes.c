#include "device_electrodes.h"
#include "sdk_error.h"
#include "celectrode-state-channel.h"
#include "utils.h"

#include <stdio.h>

#define SIGNAL_SAMPLES_COUNT 20

typedef struct _ChannelData {
    ElectrodeState data[SIGNAL_SAMPLES_COUNT];
    // The position of the data read cursor in the channel from the moment the channel was created
    size_t channelDataOffset;
    // data buffer is full
    bool dataFilled;
} ChannelData;

void electrodes_channel_callback(AnyChannel* channel, size_t length, void* electrodesDataOut) {
    ChannelData* data = (ChannelData*)electrodesDataOut;
    if (!data->dataFilled && (length - data->channelDataOffset) >= SIGNAL_SAMPLES_COUNT) {
        size_t samples_read = 0;
        if (ElectrodeStateChannel_read_data((ElectrodeStateChannel*)channel, data->channelDataOffset, SIGNAL_SAMPLES_COUNT, data->data, SIGNAL_SAMPLES_COUNT, &samples_read) == SDK_NO_ERROR) {
            if (samples_read == SIGNAL_SAMPLES_COUNT) {
                data->channelDataOffset = data->channelDataOffset + samples_read;
                data->dataFilled = true;
            }
        }
    }
}

void device_electrodes_mode(Device* device) {
    printf("-------[Device electrodes state]------------\n");
    
    ElectrodeStateChannel* elStateChannel = create_ElectrodeStateChannel(device);

    int resultCode = device_execute(device, CommandStartSignal);
    if (resultCode != SDK_NO_ERROR) {
        char errorMsg[1024];
        sdk_last_error_msg(errorMsg, 1024);
        printf("Cannot execute StartSignal command: %s\n", errorMsg);
    }
    else {
        LengthListenerHandle elStateChannelListener = NULL;
        ChannelData elStateData;
        elStateData.channelDataOffset = 0;
        elStateData.dataFilled = false;

        AnyChannel_add_length_callback((AnyChannel*)elStateChannel, electrodes_channel_callback, &elStateChannelListener, &elStateData);

        int attempts = 5;
        do {
            while (!elStateData.dataFilled) {
                demo_sleep_ms(10);
            }
            
            for (size_t i = 0; i < SIGNAL_SAMPLES_COUNT; ++i) {
                    char* data = NULL;
                    switch (elStateData.data[i]) {
                    case ElectrodeStateNormal:
                        data = "Normal";
                        break;
                    case ElectrodeStateHighResistance:
                        data = "HighResistance";
                        break;
                    case ElectrodeStateDetached:
                        data = "Detached";
                        break;
                    default:
                        data = "Unknown";
                        break;
                    }
                    printf("Electrodes state: %s\n", data);
                
            }
            elStateData.dataFilled = false;
        } while (attempts-- > 0);

        resultCode = device_execute(device, CommandStopSignal);
        if (resultCode != SDK_NO_ERROR) {
            char errorMsg[1024];
            sdk_last_error_msg(errorMsg, 1024);
            printf("Cannot execute StopSignal command: %s\n", errorMsg);
        }

        free_length_listener_handle(elStateChannelListener);
    }
    
    AnyChannel_delete((AnyChannel*)elStateChannel);
    printf("-----------------------------------\n");
}