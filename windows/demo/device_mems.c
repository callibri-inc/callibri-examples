#include "device_mems.h"
#include "cmems-channel.h"
#include <stdio.h>
#include "sdk_error.h"

#define SIGNAL_SAMPLES_COUNT 20

typedef struct _ChannelData {
    MEMS data[SIGNAL_SAMPLES_COUNT];
    size_t channelDataOffset;
    bool dataFilled;
} ChannelData;

void mems_callback(AnyChannel* channel, size_t length, void* memsDataOut) {
    ChannelData* data = (ChannelData*)memsDataOut;
    if (!data->dataFilled && (length - data->channelDataOffset) >= SIGNAL_SAMPLES_COUNT) {
        size_t samples_read = 0;
        if (MEMSChannel_read_data((MEMSChannel*)channel, data->channelDataOffset, SIGNAL_SAMPLES_COUNT, data->data, SIGNAL_SAMPLES_COUNT, &samples_read) == SDK_NO_ERROR) {
            if (samples_read == SIGNAL_SAMPLES_COUNT) {
                data->channelDataOffset = data->channelDataOffset + samples_read;
                data->dataFilled = true;
            }
        }
    }
}

void device_mems_mode(Device* device) {
	printf("-------[MEMS]------------\n");

	MEMSChannel* memsChannel = create_MEMSChannel(device);
	int resultCode = device_execute(device, CommandStartMEMS);
	if (resultCode != SDK_NO_ERROR) {
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot execute StartMEMS command: %s\n", errorMsg);
	}
	else {
        LengthListenerHandle memsChannelListener = NULL;

        ChannelData memsData;
        memsData.channelDataOffset = 0;
        memsData.dataFilled = false;

        AnyChannel_add_length_callback((AnyChannel*)memsChannel, mems_callback, &memsChannelListener, &memsData);

        int attempts = 5;
        do {
            while (!memsData.dataFilled) {
                demo_sleep_ms(10);
            }

            printf("[%d MEMS samples are received.]\n", SIGNAL_SAMPLES_COUNT);
            printf("     Sample:\n");
            for (size_t i = 0; i < SIGNAL_SAMPLES_COUNT; ++i) {
                printf("Accelerometer: X = %.2f\tY = %.2f\tZ = %.2f\nGyroscope: X = %.2f\tY = %.2f\tZ = %.2f\n\n", 
                    memsData.data[i].accelerometer.X, memsData.data[i].accelerometer.Y, memsData.data[i].accelerometer.Z, 
                    memsData.data[i].gyroscope.X, memsData.data[i].gyroscope.Y, memsData.data[i].gyroscope.Z);
            }
            memsData.dataFilled = false;
        } while (attempts-- > 0);

        resultCode = device_execute(device, CommandStopMEMS);
        if (resultCode != SDK_NO_ERROR) {
            char errorMsg[1024];
            sdk_last_error_msg(errorMsg, 1024);
            printf("Cannot execute StopMEMS command: %s\n", errorMsg);
        }

        free_length_listener_handle(memsChannelListener);
	}

    AnyChannel_delete((AnyChannel*)memsChannel);
    printf("-----------------------------------\n");

}