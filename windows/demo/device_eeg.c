#include "device_eeg.h"
#include "c_eeg_channels.h"
#include "cspectrum_power_channel.h"
#include "cparams.h"
#include "utils.h"
#include "sdk_error.h"
#include "device_params_setter.h"

#define SIGNAL_SAMPLES_COUNT 20

typedef struct _ChannelData {
    double data[SIGNAL_SAMPLES_COUNT];
    size_t channelDataOffset;
    bool dataFilled;
} ChannelData;

void spectrumPower_channel_callback(AnyChannel* channel, size_t length, void* channelDataOut) {
    ChannelData* data = (ChannelData*)channelDataOut;
    if (!data->dataFilled && (length - data->channelDataOffset) >= SIGNAL_SAMPLES_COUNT) {
        size_t samples_read = 0;
        if (DoubleChannel_read_data((DoubleChannel*)channel, data->channelDataOffset, SIGNAL_SAMPLES_COUNT, data->data, SIGNAL_SAMPLES_COUNT, &samples_read) == SDK_NO_ERROR) {
            if (samples_read == SIGNAL_SAMPLES_COUNT) {
                data->channelDataOffset = data->channelDataOffset + samples_read;
                data->dataFilled = true;
            }
        }
    }
}

void device_eeg_mode(Device* device) {
	printf("-------[EEG Indexes]------------\n");

    //Set parameters
    int resultCode = set_params_to_mode(device, DemoModeEEGIndexes);
    if (resultCode != SDK_NO_ERROR) {
        printf("Something went wrong!");
        return;
    }
    //----------------------------------------
    
    EegDoubleChannel* eegchannel = create_EegDoubleChannel(device);
    SpectrumDoubleChannel* spectrumChannel = create_SpectrumDoubleChannel(eegchannel);

    SpectrumDoubleChannel* channels[1] = { spectrumChannel };
    SpectrumPowerDoubleChannel* spectrumPowerAlphaChannel = create_SpectrumPowerDoubleChannel(channels, 1, 8.0, 14.0, "alpha", 8.0, 0.95);
    SpectrumPowerDoubleChannel* spectrumPowerBetaChannel = create_SpectrumPowerDoubleChannel(channels, 1, 14.0, 24.0, "beta", 8.0, 0.95);
    SpectrumPowerDoubleChannel* spectrumPowerDeltaChannel = create_SpectrumPowerDoubleChannel(channels, 1, 0.5, 4.0, "delta", 8.0, 0.95);
    SpectrumPowerDoubleChannel* spectrumPowerThetaChannel = create_SpectrumPowerDoubleChannel(channels, 1, 4.0, 8.0, "theta", 8.0, 0.95);

    resultCode = device_execute(device, CommandStartSignal);
    if (resultCode != SDK_NO_ERROR) {
        char errorMsg[1024];
        sdk_last_error_msg(errorMsg, 1024);
        printf("Cannot execute StartSignal command: %s\n", errorMsg);
    }
    else {
        LengthListenerHandle alphaListener = NULL;
        LengthListenerHandle betaListener = NULL;
        LengthListenerHandle deltaListener = NULL;
        LengthListenerHandle thetaListener = NULL;

        ChannelData alphaData;
        alphaData.channelDataOffset = 0;
        alphaData.dataFilled = false;
        ChannelData betaData;
        betaData.channelDataOffset = 0;
        betaData.dataFilled = false;
        ChannelData deltaData;
        deltaData.channelDataOffset = 0;
        deltaData.dataFilled = false;
        ChannelData thetaData;
        thetaData.channelDataOffset = 0;
        thetaData.dataFilled = false;

        AnyChannel_add_length_callback((AnyChannel*)spectrumPowerAlphaChannel, spectrumPower_channel_callback, &alphaListener, &alphaData);
        AnyChannel_add_length_callback((AnyChannel*)spectrumPowerBetaChannel, spectrumPower_channel_callback, &betaListener, &betaData);
        AnyChannel_add_length_callback((AnyChannel*)spectrumPowerDeltaChannel, spectrumPower_channel_callback, &deltaListener, &deltaData);
        AnyChannel_add_length_callback((AnyChannel*)spectrumPowerThetaChannel, spectrumPower_channel_callback, &thetaListener, &thetaData);

        int attempts = 5;
        do {
            while (!alphaData.dataFilled
                || !betaData.dataFilled
                || !deltaData.dataFilled
                || !thetaData.dataFilled) {
                demo_sleep_ms(500);
            }

            printf("[%d spectrum power samples are received.]\n", SIGNAL_SAMPLES_COUNT);
            printf("     Alpha:\tBeta:\tDelta:\tTheta:\t\n");
            for (size_t i = 0; i < SIGNAL_SAMPLES_COUNT; ++i) {
                printf("\t%.2f\t%.2f\t%.2f\t%.2f\n",
                    alphaData.data[i] * 1e6,
                    betaData.data[i] * 1e6,
                    deltaData.data[i] * 1e6,
                    thetaData.data[i] * 1e6);
            }
            // Reset data
            alphaData.dataFilled = false;
            betaData.dataFilled = false;
            deltaData.dataFilled = false;
            thetaData.dataFilled = false;
        } while (attempts-- > 0);

        resultCode = device_execute(device, CommandStopSignal);
        if (resultCode != SDK_NO_ERROR) {
            char errorMsg[1024];
            sdk_last_error_msg(errorMsg, 1024);
            printf("Cannot execute StopSignal command: %s\n", errorMsg);
        }

        free_length_listener_handle(alphaListener);
        free_length_listener_handle(betaListener);
        free_length_listener_handle(deltaListener);
        free_length_listener_handle(thetaListener);

    }

    AnyChannel_delete((AnyChannel*)spectrumPowerAlphaChannel);
    AnyChannel_delete((AnyChannel*)spectrumPowerBetaChannel);
    AnyChannel_delete((AnyChannel*)spectrumPowerDeltaChannel);
    AnyChannel_delete((AnyChannel*)spectrumPowerThetaChannel);

	printf("-------------------\n");
}