#include "device_battery.h"
#include "sdk_error.h"
#include "cbattery-channel.h"
#include "utils.h"

#include <stdio.h>

void battery_charge_callback(Device* device, ChannelInfo channelInfo, IntDataArray batteryData, void* batteryChargeOut) {
    if (batteryData.samples_count > 0) {
        (*(int*)batteryChargeOut) = batteryData.data_array[batteryData.samples_count - 1];
    }
    free_IntDataArray(batteryData);
}

void device_battery_mode(Device* device) {
    printf("-------[Device battery]------------\n");
    ChannelInfoArray deviceChannels;
    int resultCode = device_available_channels(device, &deviceChannels);
    if (resultCode != SDK_NO_ERROR) {
        char errorMsg[1024];
        sdk_last_error_msg(errorMsg, 1024);
        printf("Cannot get device channels info: %s\n", errorMsg);
        return;
    }
    int batteryCharge = -1;
    IntDataListenerHandle batteryListener = NULL;
    for (size_t i = 0; i < deviceChannels.info_count; ++i) {
        if (deviceChannels.info_array[i].type == ChannelTypeBattery) {
            resultCode = device_subscribe_int_channel_data_received(device, deviceChannels.info_array[i], &battery_charge_callback, &batteryListener, &batteryCharge);
            if (resultCode != SDK_NO_ERROR) {
                char errorMsg[1024];
                sdk_last_error_msg(errorMsg, 1024);
                printf("Cannot subscribe battery channel notifications: %s\n", errorMsg);
                return;
            }
            break;
        }
    }
    free_ChannelInfoArray(deviceChannels);
    printf("May take a long time...\n");
    while (batteryCharge < 0) {
        //waiting for battery charge
        demo_sleep_ms(10);
    }
    free_length_listener_handle(batteryListener);
    printf("Battery charge: [%d%%]\n", batteryCharge);
    printf("-----------------------------------\n");
}