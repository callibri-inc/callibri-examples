#include "device_envelope.h"
#include "sdk_error.h"
#include "csignal-channel.h"
#include "device_params_setter.h"
#include <stdio.h>
#include <string.h>
#include "utils.h"

#define SIGNAL_SAMPLES_COUNT 20

typedef struct _ChannelData {
	double data[SIGNAL_SAMPLES_COUNT];
	// The position of the data read cursor in the channel from the moment the channel was created
	size_t channelDataOffset;
	// data buffer is full
	bool dataFilled;
} ChannelData;

void data_envelope_channel_callback(AnyChannel* channel, size_t length, void* channelDataOut) {
	ChannelData* data = (ChannelData*)channelDataOut;
	if (!data->dataFilled && (length - data->channelDataOffset) >= SIGNAL_SAMPLES_COUNT) {
		size_t samples_read = 0;
		if (DoubleChannel_read_data((DoubleChannel*)channel, data->channelDataOffset, SIGNAL_SAMPLES_COUNT, data->data, SIGNAL_SAMPLES_COUNT, &samples_read) == SDK_NO_ERROR) {
			if (samples_read == SIGNAL_SAMPLES_COUNT) {
				data->channelDataOffset = data->channelDataOffset + samples_read;
				data->dataFilled = true;
			}
		}
	}
}

void device_envelope_mode(Device* device) {
	printf("-----------[Device envelope]---------\n");

	//Setup parameters
	int resultCode = set_params_to_mode(device, DemoModeDeviceEnvelope);
	if (resultCode != SDK_NO_ERROR) {
		printf("Something went wrong!");
		return;
	}
	//--------------------------

	ChannelInfoArray deviceChannels;
	resultCode = device_available_channels(device, &deviceChannels);
	if (resultCode != SDK_NO_ERROR) {
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot get device channels info: %s\n", errorMsg);
		return;
	}

	EnvelopeChannel* envelope = NULL;
	for (size_t i = 0; i < deviceChannels.info_count; ++i) {
		if (deviceChannels.info_array[i].type == ChannelTypeEnvelope) {
			envelope = create_EnvelopeDoubleChannel(device);
			break;
		}
	}
	free_ChannelInfoArray(deviceChannels);

	resultCode = device_execute(device, CommandStartEnvelope);
	if (resultCode != SDK_NO_ERROR) {
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot execute StartEnvelope command: %s\n", errorMsg);
	}
	else {
		LengthListenerHandle envelopeListener = NULL;

		ChannelData data;
		data.channelDataOffset = 0;
		data.dataFilled = false;

		AnyChannel_add_length_callback((AnyChannel*)envelope, data_envelope_channel_callback, &envelopeListener, &data);

		int attempts = 20;
		do {
			while (!data.dataFilled) {
				demo_sleep_ms(10);
			}

			printf("[%d envelope samples are received.]\n", SIGNAL_SAMPLES_COUNT);
			printf("     Sample:\n");
			for (size_t i = 0; i < SIGNAL_SAMPLES_COUNT; ++i) {
				printf("%.2fuV\n",
					data.data[i] * 1e6);
			}
			// Reset data
			data.dataFilled = false;
		} while (attempts-- > 0);

		resultCode = device_execute(device, CommandStopEnvelope);
		if (resultCode != SDK_NO_ERROR) {
			char errorMsg[1024];
			sdk_last_error_msg(errorMsg, 1024);
			printf("Cannot execute StopEnvelope command: %s\n", errorMsg);
		}

		free_length_listener_handle(envelopeListener);
	}

	AnyChannel_delete((AnyChannel*)envelope);

	printf("-----------------------------------\n");
}