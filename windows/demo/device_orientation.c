#include "device_orientation.h"
#include "corientation-channel.h"
#include <stdio.h>
#include "sdk_error.h"

#define SIGNAL_SAMPLES_COUNT 20

typedef struct _ChannelData {
    Quaternion data[SIGNAL_SAMPLES_COUNT];
    size_t channelDataOffset;
    bool dataFilled;
} ChannelData;

void orientation_callback(AnyChannel* channel, size_t length, void* orientationDataOut) {
    ChannelData* data = (ChannelData*)orientationDataOut;
    if (!data->dataFilled && (length - data->channelDataOffset) >= SIGNAL_SAMPLES_COUNT) {
        size_t samples_read = 0;
        if (OrientationChannel_read_data((OrientationChannel*)channel, data->channelDataOffset, SIGNAL_SAMPLES_COUNT, data->data, SIGNAL_SAMPLES_COUNT, &samples_read) == SDK_NO_ERROR) {
            if (samples_read == SIGNAL_SAMPLES_COUNT) {
                data->channelDataOffset = data->channelDataOffset + samples_read;
                data->dataFilled = true;
            }
        }
    }
}

void device_orientation_mode(Device* device) {
	printf("-------[Orientation]------------\n");

	OrientationChannel* orientationChannel = create_OrientationChannel(device);
	int resultCode = device_execute(device, CommandStartAngle);
	if (resultCode != SDK_NO_ERROR) {
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot execute StartAngle command: %s\n", errorMsg);
	}
	else {
        LengthListenerHandle orientationChannelListener = NULL;

        ChannelData angleData;
        angleData.channelDataOffset = 0;
        angleData.dataFilled = false;

        AnyChannel_add_length_callback((AnyChannel*)orientationChannel, orientation_callback, &orientationChannelListener, &angleData);

        int attempts = 5;
        do {
            while (!angleData.dataFilled) {
                demo_sleep_ms(10);
            }

            printf("[%d orientation samples are received.]\n", SIGNAL_SAMPLES_COUNT);
            printf("     Sample:\n");
            for (size_t i = 0; i < SIGNAL_SAMPLES_COUNT; ++i) {
                printf("W = %.4f\nX = %.4f\nY = %.4f\nZ = %.4f\n\n", 
                    angleData.data[i].W,
                    angleData.data[i].X,
                    angleData.data[i].Y,
                    angleData.data[i].Z);
            }
            angleData.dataFilled = false;
        } while (attempts-- > 0);

        resultCode = device_execute(device, CommandStopAngle);
        if (resultCode != SDK_NO_ERROR) {
            char errorMsg[1024];
            sdk_last_error_msg(errorMsg, 1024);
            printf("Cannot execute StopRespiration command: %s\n", errorMsg);
        }

        free_length_listener_handle(orientationChannelListener);
	}

    AnyChannel_delete((AnyChannel*)orientationChannel);
    printf("-----------------------------------\n");

}