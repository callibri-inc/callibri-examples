cmake_minimum_required(VERSION 3.14)
set(PRJ_NAME "CallibriDemo")

project(${PRJ_NAME} LANGUAGES C)


if(NOT SDK_PATH)
	set(SDK_PATH "${CMAKE_CURRENT_SOURCE_DIR}/lib")
endif()

if(NOT EXISTS ${SDK_PATH})
	message(FATAL_ERROR "[Requires SDK path] [Example: -DSDK_PATH=\"C:\\NeuroSDK\"] [SDK Download: https://sdk.brainbit.com]")
else()
	message(STATUS "[SDK Path]:[${SDK_PATH}]")
endif()

if (${CMAKE_SIZEOF_VOID_P} MATCHES 8)
	set(LIB_NEUROSDK "neurosdk-x64")
else()
	set(LIB_NEUROSDK "neurosdk-x86")
endif()

add_executable(${PRJ_NAME} 
	"main.c"
	"utils.c"
	"device_finder.c"
	"device_info.c"
	"device_battery.c"
	"device_signal.c" 
	"device_electrodes.c" 
	"device_respiration.c" 
	"device_ecg.c" 
	"device_params_setter.c" 
	"device_orientation.c" 
	"device_mems.c" 
	"device_eeg.c"
	"device_envelope.c"
	"device_stimulation.c")
target_include_directories(${PRJ_NAME} 
PUBLIC 
	"include"
PRIVATE 
	"${SDK_PATH}/include"
)

if(WIN32)
	target_link_directories(${PRJ_NAME} PRIVATE "${SDK_PATH}/windows") # .lib
	find_file(SDK_DLL NAMES "${LIB_NEUROSDK}.dll" HINTS "${SDK_PATH}/windows" REQUIRED NO_CMAKE_FIND_ROOT_PATH) # .dll
endif(WIN32)

target_link_libraries(${PRJ_NAME} PRIVATE ${LIB_NEUROSDK})		
add_custom_command(TARGET ${PRJ_NAME} POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy ${SDK_DLL} $<TARGET_FILE_DIR:${PRJ_NAME}>)