#include "device_params_setter.h"
#include "cparams.h"
#include "sdk_error.h"

int set_gain(Device* device, Gain gain) {
    int resultCode = device_set_Gain(device, gain);
    if (resultCode != SDK_NO_ERROR) {
        char errorMsg[1024];
        sdk_last_error_msg(errorMsg, 1024);
        printf("Cannot set gain: %s\n", errorMsg);
    }
    return resultCode;
}

int set_offset(Device* device, unsigned char offset) {
    int resultCode = device_set_Offset(device, offset);
    if (resultCode != SDK_NO_ERROR) {
        char errorMsg[1024];
        sdk_last_error_msg(errorMsg, 1024);
        printf("Cannot set offset: %s\n", errorMsg);
    }
    return resultCode;
}

int set_ADCInputState(Device* device, ADCInput adcInput) {
    int resultCode = device_set_ADCInputState(device, adcInput);
    if (resultCode != SDK_NO_ERROR) {
        char errorMsg[1024];
        sdk_last_error_msg(errorMsg, 1024);
        printf("Cannot set ADCInput: %s\n", errorMsg);
    }
    return resultCode;
}

int set_samplingFrequency(Device* device, SamplingFrequency samplingFrequency) {
    int resultCode = device_set_SamplingFrequency(device, samplingFrequency);
    if (resultCode != SDK_NO_ERROR) {
        char errorMsg[1024];
        sdk_last_error_msg(errorMsg, 1024);
        printf("Cannot set SamplingFrequency: %s\n", errorMsg);
    }
    return resultCode;
}

int set_hardwareFilterState(Device* device, bool state) {
    int resultCode = device_set_HardwareFilterState(device, state);
    if (resultCode != SDK_NO_ERROR) {
        char errorMsg[1024];
        sdk_last_error_msg(errorMsg, 1024);
        printf("Cannot set hardware filter state: %s\n", errorMsg);
    }
    return resultCode;
}

int set_externalSwitchState(Device* device, ExternalSwitchInput externalSwitchInput) {
    int resultCode = device_set_ExternalSwitchState(device, externalSwitchInput);
    if (resultCode != SDK_NO_ERROR) {
        char errorMsg[1024];
        sdk_last_error_msg(errorMsg, 1024);
        printf("Cannot set external switch state: %s\n", errorMsg);
    }
    return resultCode;
}

int set_params_to_mode(Device* device, DemoMode mode) {
    int resultCode = 0;
    switch (mode) {
    case DemoModeDeviceInfo:
    {
        break;
    }
    case DemoModeDeviceBattery:
    {
        break;
    }
    case DemoModeDeviceElectrodes:
    {
        break;
    }
    case DemoModeDeviceSignal:
    {
        resultCode += set_gain(device, Gain6);
        resultCode += set_offset(device, 3);
        resultCode += set_ADCInputState(device, ADCInputResistance);
        resultCode += set_samplingFrequency(device, SamplingFrequencyHz125);
        resultCode += set_hardwareFilterState(device, true);
        resultCode += set_externalSwitchState(device, ExternalSwitchInputMioElectrodes);
        break;
    }
    case DemoModeDeviceEnvelope:
    {
        resultCode += set_gain(device, Gain6);
        resultCode += set_offset(device, 3);
        resultCode += set_ADCInputState(device, ADCInputResistance);
        resultCode += set_hardwareFilterState(device, true);
        resultCode += set_externalSwitchState(device, ExternalSwitchInputMioElectrodes);
        break;
    }
    case DemoModeRespiration:
    {
        resultCode += set_gain(device, Gain6);
        resultCode += set_offset(device, 3);
        resultCode += set_hardwareFilterState(device, true);
        resultCode += set_externalSwitchState(device, ExternalSwitchInputRespUSB);
        break;
    }
    case DemoModeECG:
    {
        resultCode += set_gain(device, Gain6);
        resultCode += set_offset(device, 3);
        resultCode += set_ADCInputState(device, ADCInputResistance);
        resultCode += set_samplingFrequency(device, SamplingFrequencyHz125);
        resultCode += set_externalSwitchState(device, ExternalSwitchInputMioElectrodes);
        break;
    }
    case DemoModeOrientation:
    {
        //
        break;
    }
    case DemoModeMEMS:
    {
        //
        break;
    }
    case DemoModeEEGIndexes:
    {
        resultCode += set_gain(device, Gain6);
        resultCode += set_offset(device, 3);
        resultCode += set_ADCInputState(device, ADCInputResistance);
        resultCode += set_samplingFrequency(device, SamplingFrequencyHz250);
        resultCode += set_hardwareFilterState(device, true);
        resultCode += set_externalSwitchState(device, ExternalSwitchInputMioElectrodes);
        break;
    }
    default:
        break;
    }
    return resultCode;
}