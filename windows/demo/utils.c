#include "utils.h"
#include "stdio.h"

int read_console_value(char* val) {
    if (gets_s(val, (rsize_t)sizeof val) != NULL) {
        if (strcmp(val, "q") == 0)
        {
            return 2;
        }
        else {
            val[31] = '\0';
            return 1;
        }
    }
    return 0;
}

int read_demo_mode_number(DemoMode* mode) {
    printf("-----------------------------------\n");
    printf("[  info] Device info\n");
    printf("[ power] Device battery\n");
    printf("[   elc] Device electrodes\n");
    printf("[   sig] Device raw signal\n");
    printf("[   env] Device envelope signal\n");
    printf("[  resp] Device respiration\n");
    printf("[   ecg] Device ECG mode\n");
    printf("[   ang] Device Orientation\n");
    printf("[   mms] Device MEMS\n");
    printf("[   eeg] Device EEG indexes\n");
    printf("[stimul] Device stimulation\n");
    printf("-----------------------------------\n");
    printf("Enter a [name] of demo mode or \"q\" to exit.\n");
    char val[32];
    int resCode = read_console_value(val);
    if (resCode == 1) {
        if (strcmp(val, "info") == 0) {
            (*mode) = DemoModeDeviceInfo;
        }
        if (strcmp(val, "power") == 0) {
            (*mode) = DemoModeDeviceBattery;
        }
        if (strcmp(val, "elc") == 0) {
            (*mode) = DemoModeDeviceElectrodes;
        }
        if (strcmp(val, "sig") == 0) {
            (*mode) = DemoModeDeviceSignal;
        }
        if (strcmp(val, "env") == 0) {
            (*mode) = DemoModeDeviceEnvelope;
        }
        if (strcmp(val, "resp") == 0) {
            (*mode) = DemoModeRespiration;
        }
        if (strcmp(val, "ecg") == 0) {
            (*mode) = DemoModeECG;
        }
        if (strcmp(val, "ang") == 0) {
            (*mode) = DemoModeOrientation;
        }
        if (strcmp(val, "mms") == 0) {
            (*mode) = DemoModeMEMS;
        }
        if (strcmp(val, "eeg") == 0) {
            (*mode) = DemoModeEEGIndexes;
        }
        if (strcmp(val, "stimul") == 0) {
            (*mode) = DemoModeStimulation;
        }
    }
    return resCode;
}


#ifdef WIN32
#include <windows.h>
#include <synchapi.h>
void demo_sleep_ms(uint32_t ms) {
    Sleep(ms);
}
#else
#include <unistd.h>
void demo_sleep_ms(uint32_t ms) {
    usleep(ms);
}
#endif // WIN32
//MutexSimple create_mutex() {
//    return CreateMutex(
//        NULL,              // default security attributes
//        FALSE,             // initially not owned
//        NULL);             // unnamed mutex
//}
//
//void free_mutex(MutexSimple* mutex) {
//    CloseHandle(*mutex);
//}
//
//bool lock_mutex(MutexSimple* mutex) {
//    return WaitForSingleObject(
//        *mutex,    // handle to mutex
//        INFINITE) == WAIT_OBJECT_0;  // no time-out interval
//}
//
//bool unlcok_mutex(MutexSimple* mutex) {
//    return ReleaseMutex(*mutex);
//}