#include "device_ecg.h"
#include "sdk_error.h"
#include "c_ecg_channels.h"
#include "device_params_setter.h"
#include <stdio.h>
#include "utils.h"

#define SAMPLES_COUNT 20

typedef struct _IntChannelData {
    int data[SAMPLES_COUNT];
    size_t channelDataOffset;
    bool dataFilled;
} IntChannelData;

typedef struct _DoubleChannelData {
    double data[SAMPLES_COUNT];
    size_t channelDataOffset;
    bool dataFilled;
} DoubleChannelData;

void heartRate_channel_callback(AnyChannel* channel, size_t length, void* hRateDataOut) {
    IntChannelData* data = (IntChannelData*)hRateDataOut;
    if (!data->dataFilled && (length - data->channelDataOffset) >= SAMPLES_COUNT) {
        size_t samples_read = 0;
        if (IntChannel_read_data((HeartRateIntChannel*)channel, data->channelDataOffset, SAMPLES_COUNT, data->data, SAMPLES_COUNT, &samples_read) == SDK_NO_ERROR) {
            if (samples_read == SAMPLES_COUNT) {
                data->channelDataOffset = data->channelDataOffset + samples_read;
                data->dataFilled = true;
            }
        }
    }
}

void stresIndex_channel_callback(AnyChannel* channel, size_t length, void* stressIndexDataOut) {
    DoubleChannelData* data = (DoubleChannelData*)stressIndexDataOut;
    if (!data->dataFilled && (length - data->channelDataOffset) >= SAMPLES_COUNT) {
        size_t samples_read = 0;
        if (DoubleChannel_read_data((StressIndexDoubleChannel*)channel, data->channelDataOffset, SAMPLES_COUNT, data->data, SAMPLES_COUNT, &samples_read) == SDK_NO_ERROR) {
            if (samples_read == SAMPLES_COUNT) {
                data->channelDataOffset = data->channelDataOffset + samples_read;
                data->dataFilled = true;
            }
        }
    }
}

void device_ecg_mode(Device* device) {
	printf("------------[ECG data]-------------\n");

    //Set parameters
    int resultCode = set_params_to_mode(device, DemoModeECG);
    if (resultCode != SDK_NO_ERROR) {
        printf("Something went wrong!");
        return;
    }
    //----------------------------------------

    //Create Signal channel
    ChannelInfoArray deviceChannels;
    resultCode = device_available_channels(device, &deviceChannels);
    if (resultCode != SDK_NO_ERROR) {
        char errorMsg[1024];
        sdk_last_error_msg(errorMsg, 1024);
        printf("Cannot get device channels info: %s\n", errorMsg);
        return;
    }

    SignalDoubleChannel* signal = NULL;
    for (size_t i = 0; i < deviceChannels.info_count; ++i) {
        if (deviceChannels.info_array[i].type == ChannelTypeSignal) {
            signal = create_SignalDoubleChannel_info(device, deviceChannels.info_array[i]);
            break;
        }
    }
    free_ChannelInfoArray(deviceChannels);
    //-----------------------------------------

    ElectrodeStateChannel* elStateChannel = create_ElectrodeStateChannel(device);
    RPeakChannel* rpeakChannel = create_RPeakChannel(signal, elStateChannel);

    HeartRateIntChannel* heartRateIntChannel = create_HeartRateIntChannel(rpeakChannel);
    LengthListenerHandle heartRateListener = NULL;
    IntChannelData heartRateData;
    heartRateData.channelDataOffset = 0;
    heartRateData.dataFilled = false;
    AnyChannel_add_length_callback((AnyChannel*)heartRateIntChannel, heartRate_channel_callback, &heartRateListener, &heartRateData);

    StressIndexDoubleChannel* stressIndexDoubleChannel = create_StressIndexDoubleChannel(rpeakChannel);
    LengthListenerHandle stressIndexListener = NULL;
    DoubleChannelData stressIndexData;
    stressIndexData.channelDataOffset = 0;
    stressIndexData.dataFilled = false;
    AnyChannel_add_length_callback((AnyChannel*)stressIndexDoubleChannel, stresIndex_channel_callback, &stressIndexListener, &stressIndexData);

    resultCode = device_execute(device, CommandStartSignal);
    if (resultCode != SDK_NO_ERROR) {
        char errorMsg[1024];
        sdk_last_error_msg(errorMsg, 1024);
        printf("Cannot execute StartSignal command: %s\n", errorMsg);
    }
    else {
        int attempts = 5;
        do {
            while (!stressIndexData.dataFilled || !heartRateData.dataFilled) {
                demo_sleep_ms(10);
            }

            printf("[%d Heart rate are received.]\n", SAMPLES_COUNT);
            printf("     Sample:\n");
            for (size_t i = 0; i < SAMPLES_COUNT; ++i) {
                printf("%d\n",
                    heartRateData.data[i]);
            }

            printf("[%d Stress index are received.]\n", SAMPLES_COUNT);
            printf("     Sample:\n");
            for (size_t i = 0; i < SAMPLES_COUNT; ++i) {
                printf("%.2f\n",
                    stressIndexData.data[i]);
            }

            heartRateData.dataFilled = false;
            stressIndexData.dataFilled = false;

        } while (attempts-- > 0);
                
        resultCode = device_execute(device, CommandStopSignal);
        if (resultCode != SDK_NO_ERROR) {
            char errorMsg[1024];
            sdk_last_error_msg(errorMsg, 1024);
            printf("Cannot execute StopSignal command: %s\n", errorMsg);
        }
        free_length_listener_handle(heartRateListener);
        free_length_listener_handle(stressIndexListener);
    }

    AnyChannel_delete((AnyChannel*)heartRateIntChannel);
    AnyChannel_delete((AnyChannel*)stressIndexDoubleChannel);
    AnyChannel_delete((AnyChannel*)rpeakChannel);
    AnyChannel_delete((AnyChannel*)signal);
    AnyChannel_delete((AnyChannel*)elStateChannel);

	printf("-----------------------------------\n");
}