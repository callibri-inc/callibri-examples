cmake_minimum_required(VERSION 3.14)

project(CallibriData LANGUAGES C)

add_executable(CallibriData main.c)

target_include_directories(CallibriData PRIVATE include)

find_library(SDK_LIB NAMES neurosdk-x64 PATHS windows)
target_link_libraries(CallibriData PRIVATE ${SDK_LIB})

find_file(SDK_DLL NAMES neurosdk-x64.dll PATHS windows)
add_custom_command(TARGET CallibriData POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy ${SDK_DLL} $<TARGET_FILE_DIR:CallibriData>)