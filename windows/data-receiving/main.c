#include "cscanner.h"
#include "cdevice.h"
#include "cparams.h"
#include "c_eeg_channels.h"
#include "c_ecg_channels.h"
#include "csignal-channel.h"
#include "corientation-channel.h"
#include "cmems-channel.h"
#include "sdk_error.h"
#include "stdio.h"
#include "string.h"
#include "windows.h"

#define SIGNAL_SAMPLES_COUNT 80

void print_signal(Device *device, DoubleChannel *double_channel)
{
	int resultCode = device_execute(device, CommandStartSignal);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot execute StartSignal command: %s\n", errorMsg);
		return;
	}

	size_t signalLength = 0;

	do
	{
		AnyChannel_get_total_length(double_channel, &signalLength);
	} while (signalLength < SIGNAL_SAMPLES_COUNT);

	resultCode = device_execute(device, CommandStopSignal);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot execute StopSignal command: %s\n", errorMsg);
		return;
	}

	double signalBuffer[SIGNAL_SAMPLES_COUNT];

	DoubleChannel_read_data(double_channel, 0, SIGNAL_SAMPLES_COUNT, signalBuffer, SIGNAL_SAMPLES_COUNT, &signalLength);

	printf("%d signal samples are received.\n", SIGNAL_SAMPLES_COUNT);
	printf("Signal:\n");
	for (size_t i = 0; i < SIGNAL_SAMPLES_COUNT; ++i)
	{
		printf("%.2fuV\n",
			signalBuffer[i] * 1e6);
	}
}

void print_eeg_signal(Device *device, ChannelInfo channel_info)
{
	int resultCode = device_set_SamplingFrequency(device, SamplingFrequencyHz250);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot set sampling frequency: %s\n", errorMsg);
		return;
	}

	resultCode = device_set_Gain(device, Gain6);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot set gain: %s\n", errorMsg);
		return;
	}

	resultCode = device_set_Offset(device, 0);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot set offset: %s\n", errorMsg);
		return;
	}

	resultCode = device_set_ExternalSwitchState(device, ExternalSwitchInputMioUSB);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot set external switch state: %s\n", errorMsg);
		return;
	}

	resultCode = device_set_ADCInputState(device, ADCInputResistance);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot set ADC input: %s\n", errorMsg);
		return;
	}

	resultCode = device_set_HardwareFilterState(device, true);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot enable hardware filter: %s\n", errorMsg);
		return;
	}
	
	DoubleChannel* signalChannel = create_EegDoubleChannel_info(device, channel_info);
	print_signal(device, signalChannel);
	AnyChannel_delete(signalChannel);
}

void print_ecg_signal(Device* device, ChannelInfo channel_info)
{
	int resultCode = device_set_SamplingFrequency(device, SamplingFrequencyHz125);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot set sampling frequency: %s\n", errorMsg);
		return;
	}

	resultCode = device_set_Gain(device, Gain6);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot set gain: %s\n", errorMsg);
		return;
	}

	resultCode = device_set_Offset(device, 0);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot set offset: %s\n", errorMsg);
		return;
	}

	resultCode = device_set_ExternalSwitchState(device, ExternalSwitchInputMioUSB);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot set external switch state: %s\n", errorMsg);
		return;
	}

	resultCode = device_set_ADCInputState(device, ADCInputResistance);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot set ADC input: %s\n", errorMsg);
		return;
	}

	resultCode = device_set_HardwareFilterState(device, true);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot enable hardware filter: %s\n", errorMsg);
		return;
	}

	DoubleChannel* signalChannel = create_EcgDoubleChannel_info(device, channel_info);
	print_signal(device, signalChannel);
	AnyChannel_delete(signalChannel);
}

void print_myo_signal(Device* device, ChannelInfo channel_info)
{
	int resultCode = device_set_SamplingFrequency(device, SamplingFrequencyHz1000);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot set sampling frequency: %s\n", errorMsg);
		return;
	}

	resultCode = device_set_Gain(device, Gain6);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot set gain: %s\n", errorMsg);
		return;
	}

	resultCode = device_set_Offset(device, 0);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot set offset: %s\n", errorMsg);
		return;
	}

	resultCode = device_set_ExternalSwitchState(device, ExternalSwitchInputMioUSB);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot set external switch state: %s\n", errorMsg);
		return;
	}

	resultCode = device_set_ADCInputState(device, ADCInputResistance);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot set ADC input: %s\n", errorMsg);
		return;
	}

	resultCode = device_set_HardwareFilterState(device, true);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot enable hardware filter: %s\n", errorMsg);
		return;
	}

	DoubleChannel* signalChannel = create_SignalDoubleChannel_info(device, channel_info);
	print_signal(device, signalChannel);
	AnyChannel_delete(signalChannel);
}

void print_selected_signal(Device* device, void(*print_function)(Device*,ChannelInfo))
{
	printf("Receiving signal...\n");

	ChannelInfoArray deviceChannels;
	int resultCode = device_available_channels(device, &deviceChannels);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot get device channels info: %s\n", errorMsg);
		return;
	}

	for (size_t i = 0; i < deviceChannels.info_count; ++i)
	{
		if (deviceChannels.info_array[i].type == ChannelTypeSignal)
		{
			print_function(device, deviceChannels.info_array[i]);
			break;
		}
	}
	free_ChannelInfoArray(deviceChannels);	
}

Device* filter_device(DeviceEnumerator* enumerator, const DeviceInfoArray* device_info_array, char* serial_number)
{
	for (size_t i = 0; i < device_info_array->info_count; ++i)
	{
		Device* device = create_Device(enumerator, device_info_array->info_array[i]);
		if (device == NULL)
		{
			continue;
		}
		
		int resultCode = device_connect(device);
		if (resultCode != SDK_NO_ERROR)
		{
			device_delete(device);
			continue;
		}

		DeviceState deviceState;
		do
		{
			device_read_State(device, &deviceState);
		}
		while (deviceState != DeviceStateConnected);

		char deviceSerialNumber[32];
		resultCode = device_read_SerialNumber(device, deviceSerialNumber, 32);
		if (resultCode != SDK_NO_ERROR)
		{
			device_delete(device);
			continue;
		}

		if (strcmp(deviceSerialNumber, serial_number) == 0)
		{
			return device;
		}
	}

	return NULL;
}

Device* connect_to_device(char* serial_number)
{
	DeviceEnumerator* enumerator = create_device_enumerator(DeviceTypeCallibri);
	if (enumerator == NULL)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Device enumerator is null: %s\n", errorMsg);
		return NULL;
	}
	
	int attempts = 10;
	Device* device = NULL;
	do
	{
		DeviceInfoArray deviceInfoArray;
		const int resultCode = enumerator_get_device_list(enumerator, &deviceInfoArray);
		if (resultCode != SDK_NO_ERROR)
		{
			continue;
		}

		device = filter_device(enumerator, &deviceInfoArray, serial_number);
		free_DeviceInfoArray(deviceInfoArray);
		if (device == NULL)
		{
			Sleep(300);
		}
	}
	while (device == NULL && attempts-- > 0);

	enumerator_delete(enumerator);
	return device;
}

void signal_by_serial(char *serial_number, void(*print_function)(Device*, ChannelInfo))
{
	Device* device = connect_to_device(serial_number);
	if (device == NULL)
	{
		printf("Device with S/N %s not found.\n", serial_number);
		return;
	}

	char deviceAddress[32];
	int resultCode = device_read_Address(device, deviceAddress, 32);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot get address: %s\n", errorMsg);
		device_delete(device);
		return;
	}

	printf("Device SN %s found. Address: %s\n", serial_number, deviceAddress);

	print_selected_signal(device, print_function);

	device_delete(device);
}

void orientation_by_serial(char *serial_number)
{
	Device* device = connect_to_device(serial_number);
	if (device == NULL)
	{
		printf("Device with S/N %s not found.\n", serial_number);
		return;
	}

	char deviceAddress[32];
	int resultCode = device_read_Address(device, deviceAddress, 32);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot get address: %s\n", errorMsg);
		device_delete(device);
		return;
	}

	printf("Device SN %s found. Address: %s\n", serial_number, deviceAddress);

	printf("Receiving signal...\n");

	OrientationChannel* orientationChannel = create_OrientationChannel(device);
	resultCode = device_execute(device, CommandStartAngle);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot execute StartAngle command: %s\n", errorMsg);
		return;
	}

	size_t signalLength = 0;

	do
	{
		AnyChannel_get_total_length(orientationChannel, &signalLength);
	} while (signalLength < SIGNAL_SAMPLES_COUNT);

	resultCode = device_execute(device, CommandStopAngle);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot execute StopSignal command: %s\n", errorMsg);
		return;
	}

	Quaternion signalBuffer[SIGNAL_SAMPLES_COUNT];

	OrientationChannel_read_data(orientationChannel, 0, SIGNAL_SAMPLES_COUNT, signalBuffer, SIGNAL_SAMPLES_COUNT, &signalLength);

	printf("%d orientation samples are received.\n", SIGNAL_SAMPLES_COUNT);
	printf("Quaternions:\n");
	for (size_t i = 0; i < SIGNAL_SAMPLES_COUNT; ++i)
	{
		printf("w:%.2f\t x:%.2f\t y:%.2f\t z:%.2f\n",
			signalBuffer[i].W, signalBuffer[i].X, signalBuffer[i].Y, signalBuffer[i].Z);
	}
	AnyChannel_delete(orientationChannel);

	device_delete(device);
}

void mems_by_serial(char *serial_number)
{
	Device* device = connect_to_device(serial_number);
	if (device == NULL)
	{
		printf("Device with S/N %s not found.\n", serial_number);
		return;
	}

	char deviceAddress[32];
	int resultCode = device_read_Address(device, deviceAddress, 32);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot get address: %s\n", errorMsg);
		device_delete(device);
		return;
	}

	printf("Device SN %s found. Address: %s\n", serial_number, deviceAddress);

	printf("Receiving signal...\n");

	MEMSChannel* memsChannel = create_MEMSChannel(device);
	resultCode = device_execute(device, CommandStartMEMS);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot execute StartMEMS command: %s\n", errorMsg);
		return;
	}

	size_t signalLength = 0;

	do
	{
		AnyChannel_get_total_length(memsChannel, &signalLength);
	} while (signalLength < SIGNAL_SAMPLES_COUNT);

	resultCode = device_execute(device, CommandStopMEMS);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot execute StopMEMS command: %s\n", errorMsg);
		return;
	}

	MEMS signalBuffer[SIGNAL_SAMPLES_COUNT];

	MEMSChannel_read_data(memsChannel, 0, SIGNAL_SAMPLES_COUNT, signalBuffer, SIGNAL_SAMPLES_COUNT, &signalLength);

	printf("%d MEMS samples are received.\n", SIGNAL_SAMPLES_COUNT);
	printf("Samples:\n");
	for (size_t i = 0; i < SIGNAL_SAMPLES_COUNT; ++i)
	{
		printf("acc_x:%.2f deg/s\tacc_y:%.2f deg/s\tacc_z:%.2f deg/s\tgyro_x:%.2f g\tgyro_y:%.2f g\tgyro_z:%.2f g\n",
			signalBuffer[i].accelerometer.X, signalBuffer[i].accelerometer.Y, signalBuffer[i].accelerometer.Z,
			signalBuffer[i].gyroscope.X, signalBuffer[i].gyroscope.Y, signalBuffer[i].gyroscope.Z);
	}
	AnyChannel_delete(memsChannel);

	device_delete(device);
}

int main()
{
	while(true)
	{
		printf("Enter [ecg/eeg/myo/ang/mms] and a serial number of a device or \"q\" to exit.\n");
		char input[32];
		
		if (gets_s(input, (rsize_t)sizeof input) != NULL)
		{
			if (strcmp(input, "q") == 0)
			{
				break;
			}

			char typeStr[4];
			memcpy(typeStr, input, 3);
			typeStr[3] = '\0';
			
			if (strcmp(typeStr, "eeg") == 0)
			{
				char serialStr[28];
				memcpy(serialStr, &input[4], 27);
				serialStr[27] = '\0';
				signal_by_serial(serialStr, &print_eeg_signal);
				continue;
			}
			
			if (strcmp(typeStr, "ecg") == 0)
			{
				char serialStr[28];
				memcpy(serialStr, &input[4], 27);
				serialStr[27] = '\0';
				signal_by_serial(serialStr, &print_ecg_signal);
				continue;
			}
			
			if (strcmp(typeStr, "myo") == 0)
			{
				char serialStr[28];
				memcpy(serialStr, &input[4], 27);
				serialStr[27] = '\0';
				signal_by_serial(serialStr, &print_myo_signal);
				continue;
			}
			
			if (strcmp(typeStr, "ang") == 0)
			{
				char serialStr[28];
				memcpy(serialStr, &input[4], 27);
				serialStr[27] = '\0';
				orientation_by_serial(serialStr);
				continue;
			}
			
			if (strcmp(typeStr, "mms") == 0)
			{
				char serialStr[28];
				memcpy(serialStr, &input[4], 27);
				serialStr[27] = '\0';
				mems_by_serial(serialStr);
				continue;
			}

			printf("Wrong command.\n");
		}		
	}	
	
	return 0;
}