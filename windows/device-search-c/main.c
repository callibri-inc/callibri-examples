#include "cscanner.h"
#include "cdevice.h"
#include "cparams.h"
#include "sdk_error.h"
#include "stdio.h"
#include "string.h"
#include "windows.h"

Device* filter_device(DeviceEnumerator* enumerator, const DeviceInfoArray* device_info_array, char* serial_number)
{
	for (size_t i = 0; i < device_info_array->info_count; ++i)
	{
		Device* device = create_Device(enumerator, device_info_array->info_array[i]);
		if (device == NULL)
		{
			continue;
		}
		
		int resultCode = device_connect(device);
		if (resultCode != SDK_NO_ERROR)
		{
			device_delete(device);
			continue;
		}

		DeviceState deviceState;
		do
		{
			device_read_State(device, &deviceState);
		}
		while (deviceState != DeviceStateConnected);

		char deviceSerialNumber[32];
		resultCode = device_read_SerialNumber(device, deviceSerialNumber, 32);
		if (resultCode != SDK_NO_ERROR)
		{
			device_delete(device);
			continue;
		}

		if (strcmp(deviceSerialNumber, serial_number) == 0)
		{
			return device;
		}
	}

	return NULL;
}

Device* connect_to_device(char* serial_number)
{
	DeviceEnumerator* enumerator = create_device_enumerator(DeviceTypeCallibri);
	if (enumerator == NULL)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Device enumerator is null: %s\n", errorMsg);
		return NULL;
	}
	
	int attempts = 10;
	Device* device = NULL;
	do
	{
		DeviceInfoArray deviceInfoArray;
		const int resultCode = enumerator_get_device_list(enumerator, &deviceInfoArray);
		if (resultCode != SDK_NO_ERROR)
		{
			continue;
		}

		device = filter_device(enumerator, &deviceInfoArray, serial_number);
		free_DeviceInfoArray(deviceInfoArray);
		if (device == NULL)
		{
			Sleep(300);
		}
	}
	while (device == NULL && attempts-- > 0);

	enumerator_delete(enumerator);
	return device;
}

void find_by_serial(char *serial_number)
{
	Device* device = connect_to_device(serial_number);
	if (device == NULL)
	{
		printf("Device with S/N %s not found.\n", serial_number);
		return;
	}

	char deviceAddress[32];
	int resultCode = device_read_Address(device, deviceAddress, 32);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot get address: %s\n", errorMsg);
		device_delete(device);
		return;
	}
	
	resultCode = device_execute(device, CommandFindMe);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot execute FindMe command: %s\n", errorMsg);
		device_delete(device);
		return;
	}
	printf("Device SN %s found. Address: %s\n", serial_number, deviceAddress);

	device_delete(device);
}

int main()
{
	while(true)
	{
		printf("Enter a serial number of a device or \"q\" to exit.\n");
		char input[32];
		
		if (gets_s(input, (rsize_t)sizeof input) != NULL)
		{
			if (strcmp(input, "q") == 0)
			{
				break;
			}

			find_by_serial(input);
		}		
	}	
	
	return 0;
}