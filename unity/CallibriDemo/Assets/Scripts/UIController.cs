﻿using System;
using System.Collections;
using System.Collections.Generic;
using Neuro;
using Neuro.Native;
using UnityEngine;
using UnityEngine.UI;

public sealed class UIController : MonoBehaviour
{
    ChannelsController channelsController = null;
    Device device = null;

    public GameObject modesVariations;
    public GameObject deviceSearchLabel;

    [Header("== Demo buttons ==")]
    public Button DeviceInfoBtn;
    public Button SignalBtn;
    public Button EEGBtn;
    public Button ElectrodesBtn;
    public Button EMGBtn;
    public Button StimulationBtn;


    [Header("== DeviceState UI ==")]
    public Text deviceConnectionState;
    public Text devicePowerState;
    private int devicePower = 0;

    [Header("== DeviceInfo UI ==")]
    public GameObject deviceInfoOutput;
    public Text deviceInfoText;

    [Header("== Signal UI ==")]
    public GameObject signalOutput;
    public Graph signalGraph;

    [Header("== EEG UI ==")]
    public GameObject eegOutput;
    public Graph eegGraph;
    public Text alphaId;
    public Text betaId;
    public Text thetaId;
    public Text deltaId;
    private EegIndexValues indexValues = new EegIndexValues();

    [Header("== Electrodes UI ==")]
    public GameObject elOutput;
    public Text elState;
    private ElectrodesController.ElectrodeState electrodesState = ElectrodesController.ElectrodeState.Lost;

    [Header("== EMG UI ==")]
    public GameObject emgOutput;
    private int rawHRValue = 0;
    public Text hrValue;
    private double rawSIValue = 0;
    public Text siValue;

    [Header("== Stimulation UI ==")]
    public GameObject stimulOutput;
    public InputField ampField;
    public InputField pulseField;
    public InputField freqField;
    public InputField durationField;
    public Button startStimulation;
    public Button stopStimulation;
    public Button readStimulParam;
    public Button writeStimulParam;

    private void Awake()
    {
        channelsController = new ChannelsController();
    }

    private void Start()
    {
        ShowMenu(false);
    }

    private void FixedUpdate()
    {
        devicePowerState.text = string.Format("Power: {0}%", devicePower);

        alphaId.text = string.Format("Alpha: {0:F2}", indexValues.AlphaRate);
        betaId.text = string.Format("Beta: {0:F2}", indexValues.BetaRate);
        thetaId.text = string.Format("Theta: {0:F2}", indexValues.ThetaRate);
        deltaId.text = string.Format("Delta: {0:F2}", indexValues.DeltaRate);

        elState.text = string.Format("Electrodes state: {0}", electrodesState);

        hrValue.text = string.Format("Heart rate: {0}", rawHRValue);
        siValue.text = string.Format("Stress index: {0}", rawSIValue);
    }

    public void SaveDevice(Device device)
    {
        this.device = device;
    }

    #region DeviceInfo
    public void ShowDeviceInfo()
    {
        ShowMenu(false);
        deviceInfoOutput.SetActive(true);
        GetDeviceInfo();
    }

    public void CloseDeviceInfo()
    {
        ShowMenu(true);
        deviceInfoOutput.SetActive(false);
    }
    #endregion

    #region Signal
    public void ShowSignal()
    {
        ShowMenu(false);
        signalOutput.SetActive(true);
        channelsController.createSignal(device, (samples)=> {
            signalGraph.UpdateGraph(samples);
        });
        signalGraph.InitGraph(channelsController.GetSignalPlotSize());
    }

    public void CloseSignal()
    {
        ShowMenu(true);
        signalOutput.SetActive(false);
        channelsController.destroySignal(device);
        signalGraph.Close();
    }
    #endregion

    #region EEG
    public void ShowEEG() {
        ShowMenu(false);
        eegOutput.SetActive(true);
        channelsController.createEeg(device, (indexes) => {
            indexValues = indexes;
        },
        (samples) => {
            eegGraph.UpdateGraph(samples);
        });
        eegGraph.InitGraph(channelsController.GetEegPlotSize());
    }

    public void CloseEEG()
    {
        ShowMenu(true);
        eegOutput.SetActive(false);
        channelsController.destroyEeg(device);
        eegGraph.Close();
    }
    #endregion

    #region Electrodes
    public void ShowElectrodes()
    {
        ShowMenu(false);
        elOutput.SetActive(true);
        channelsController.createEl(device, (state) => {
            electrodesState = state;
        });
    }

    public void CloseElectrodes()
    {
        ShowMenu(true);
        elOutput.SetActive(false);
        channelsController.destroyEl(device);
    }
    #endregion

    #region EMG
    public void ShowEMG()
    {
        ShowMenu(false);
        emgOutput.SetActive(true);
        channelsController.createEMG(device, 
        (hr) => {
            rawHRValue = hr[0];
        },
        (si) => {
            rawSIValue = si[0];
        });
    }

    public void CloseEmg()
    {
        ShowMenu(true);
        emgOutput.SetActive(false);
        channelsController.destroyEMG(device);
    }
    #endregion

    #region Stimulation
    public void ShowStimulation()
    {
        ShowMenu(false);
        stimulOutput.SetActive(true);
    }

    public void StartStimulation()
    {
        device.Execute(Command.StartStimulation);
        stopStimulation.interactable = true;
        startStimulation.interactable = false;
    }

    public void StopStimulation()
    {
        device.Execute(Command.StopStimulation);
        stopStimulation.interactable = false;
        startStimulation.interactable = true;
    }

    public void WriteStimulationParam()
    {
        try {
            int amp = Convert.ToInt32(ampField.text);
            int pulse = Convert.ToInt32(pulseField.text);
            int frequency = Convert.ToInt32(freqField.text);
            int duration = Convert.ToInt32(durationField.text);

            StimulationParams stimulationParams = new StimulationParams()
            {
                Current = amp,
                PulseWidth = pulse,
                Frequency = frequency,
                StimulusDuration = duration
            };

            if (device != null)
            {
                device.SetParam(Parameter.StimulatorParamPack, stimulationParams);
            }

            ampField.text = "";
            pulseField.text = "";
            freqField.text = "";
            durationField.text = "";
        }
        catch (Exception ex) {
            Debug.LogError("Write stimulation params error: " + ex.Message);
        }
    }

    public void ReadStimulationParam() 
    {
        StimulationParams stimulationParams = new StimulationParams() {
            Current = 0,
            PulseWidth = 0,
            Frequency = 0,
            StimulusDuration = 0
        };
        if (device != null)
        {
            stimulationParams = device.ReadParam<StimulationParams>(Parameter.StimulatorParamPack);
        }

        ampField.text = stimulationParams.Current.ToString();
        pulseField.text = stimulationParams.PulseWidth.ToString();
        freqField.text = stimulationParams.Frequency.ToString();
        durationField.text = stimulationParams.StimulusDuration.ToString();
    }

    public void StimulParamChanged() {
        if (string.IsNullOrEmpty(ampField.text) || string.IsNullOrEmpty(pulseField.text) ||
            string.IsNullOrEmpty(freqField.text) || string.IsNullOrEmpty(durationField.text))
        {
            writeStimulParam.interactable = false;
        }
        else
        {
            writeStimulParam.interactable = true;
        }
}

    public void CloseStimulation()
    {
        ShowMenu(true);
        stimulOutput.SetActive(false);
        StopStimulation();
    }
    #endregion

    public void ShowMenu(bool enabled)
    {
        modesVariations.SetActive(enabled);
        if (enabled)
        {
            DeviceInfoBtn.interactable = true;
            if (DeviceTraits.HasChannelsWithType(device, ChannelType.Signal))
            {
                SignalBtn.interactable = true;
                EMGBtn.interactable = true;
                EEGBtn.interactable = true;
                ElectrodesBtn.interactable = true;
            }
            else
            {
                SignalBtn.interactable = false;
                EMGBtn.interactable = false;
                EEGBtn.interactable = false;
                ElectrodesBtn.interactable = false;
            }
            foreach (ParamInfo pInfo in device.Parameters)
            {
                if (pInfo.Parameter == Parameter.StimulatorParamPack)
                {
                    StimulationBtn.interactable = true;
                    return;
                }
            }
            StimulationBtn.interactable = false;
        }
    }

    public void OnDeviceStateChange(bool disconnected)
    {
        ShowMenu(!disconnected);
        deviceSearchLabel.SetActive(disconnected);
        deviceConnectionState.text = disconnected ? "Disconnected" : "Connected";

        deviceInfoOutput.SetActive(false);
        signalOutput.SetActive(false);
        eegOutput.SetActive(false);
        elOutput.SetActive(false);
        emgOutput.SetActive(false);
        stimulOutput.SetActive(false);

        if (disconnected)
        {
            channelsController.destroyBattery();
            devicePower = 0;
        }
        else
        {
            channelsController.createBattery(device, (power) => {
                devicePower = power;
            });
        }
        
    }

    private void GetDeviceInfo()
    {
        string info = "";
        info += "*Common params*\n";
        info += string.Format("Name: [{0}]\n", device.ReadParam<string>(Neuro.Native.Parameter.Name));
        info += string.Format("Address: [{0}]\n", device.ReadParam<string>(Neuro.Native.Parameter.Address));
        info += string.Format("Serial Number: [{0}]\n", device.ReadParam<string>(Neuro.Native.Parameter.SerialNumber));

        FirmwareVersion fv = device.ReadParam<FirmwareVersion>(Neuro.Native.Parameter.FirmwareVersion);
        info += string.Format("Version: [{0}.{1}]\n", fv.Version, fv.Build);

        FirmwareMode fm = device.ReadParam<FirmwareMode>(Neuro.Native.Parameter.FirmwareMode);
        info += string.Format("Mode: [{0}]\n", fm.ToString());

        info += "*Supported params*\n";
        foreach (ParamInfo paramInfo in device.Parameters)
        {
            info += string.Format("Name: [{0}] Type: [{1}] Access: [{2}]\n",
                paramInfo.Parameter.ToString(), paramInfo.Parameter.GetType(), paramInfo.Access);
        }

        info += "*Supported device channels*\n";
        foreach (ChannelInfo channelInfo in device.Channels)
        {
            info += string.Format("Name: [{0}] Type: [{1}] Index: [{2}]\n",
                channelInfo.Name, channelInfo.Type, channelInfo.Index);
        }

        info += "*Supported commands*\n";
        foreach (Command command in device.Commands)
        {
            info += string.Format("{0}\n", command);
        }
        deviceInfoText.text = info;
    }
}
