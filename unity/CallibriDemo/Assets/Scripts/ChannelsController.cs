﻿using System;
using System.Collections;
using System.Collections.Generic;
using Neuro;
using UnityEngine;

public class ChannelsController
{
    #region Battery
    Battery batteryController = null;
    public void createBattery(Device device, Action<int> onPowerChanged)
    {
        batteryController = new Battery(device);
        batteryController.onPowerChanged = onPowerChanged;
    }

    public void destroyBattery() {
        batteryController.CloseChannel();
    }
    #endregion

    #region Signal
    SignalController signalController = null;
    public void createSignal(Device device, Action<double[]> onSignalRecieved)
    {
        signalController = new SignalController(device);
        signalController.onSignalRecieved = onSignalRecieved;
    }

    public int GetSignalPlotSize() {
        return signalController.plotSize;
    }

    public void destroySignal(Device device)
    {
        signalController.CloseChannel(device);
    }
    #endregion

    #region EEG
    EegController eegController = null;
    public void createEeg(Device device, Action<EegIndexValues> onIndexesChanged, Action<double[]> onEegChanged)
    {
        eegController = new EegController(device);
        eegController.onIndexesChanged = onIndexesChanged;
        eegController.onEegChanged = onEegChanged;
    }

    public int GetEegPlotSize()
    {
        return eegController.plotSize;
    }

    public void destroyEeg(Device device)
    {
        eegController.CloseChannel(device);
    }
    #endregion

    #region Electrodes
    ElectrodesController electrodesController = null;
    public void createEl(Device device, Action<ElectrodesController.ElectrodeState> onElStateChange)
    {
        electrodesController = new ElectrodesController(device);
        electrodesController.onElChanged = onElStateChange;
    }

    public void destroyEl(Device device)
    {
        electrodesController.CloseChannel(device);
    }
    #endregion

    #region EMG
    EMGController emgController = null;
    public void createEMG(Device device,  Action<int[]> onHRChanged, Action<double[]> onSIChanged)
    {
        emgController = new EMGController(device);
        emgController.onHrRecieved = onHRChanged;
        emgController.onSiRecieved = onSIChanged;
    }

    public int GetHRPlotSize()
    {
        return emgController.HRPlotSize;
    }

    public int GetSIPlotSize()
    {
        return emgController.SIPlotSize;
    }

    public void destroyEMG(Device device)
    {
        emgController.CloseChannels(device);
    }
    #endregion
}
