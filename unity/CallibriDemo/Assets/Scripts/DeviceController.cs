﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Neuro;
using Neuro.Native;
using UnityEngine;

#if UNITY_ANDROID
using UnityEngine.Android;
#endif

public class DeviceController : MonoBehaviour
{
    public UIController uiController;
    private DeviceEnumerator deviceEnumerator = null;
    private Device device;

    bool devicefounded = false;
    DeviceInfo deviceInfo;

    bool deviceStateChanged = false;
    
    void Start()
    {
#if UNITY_ANDROID
        Permission.RequestUserPermission("android.permission.BLUETOOTH");
        Permission.RequestUserPermission("android.permission.BLUETOOTH_ADMIN");
        Permission.RequestUserPermission("android.permission.ACCESS_FINE_LOCATION");
        Permission.RequestUserPermission("android.permission.ACCESS_COARSE_LOCATION");
#endif
        createDeviceEnumerator();
    }

    private void createDeviceEnumerator()
    {
#if UNITY_IOS
        deviceEnumerator = new DeviceEnumerator(Neuro.Native.DeviceType.Callibri);
#elif UNITY_ANDROID
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject context = activity.Call<AndroidJavaObject>("getApplicationContext");
        deviceEnumerator = new DeviceEnumerator(Neuro.Native.DeviceType.Callibri, GetJniEnv(), context.GetRawObject());
#endif
        deviceEnumerator.DeviceListChanged += OnDeviceFound;
        uiController.OnDeviceStateChange(true);
    }

    private void OnDeviceFound(object sender, System.EventArgs e)
    {
        List<DeviceInfo> deviceList = new List<DeviceInfo>(deviceEnumerator.Devices);
        Debug.Log("Devices found: " + deviceList.Count);

        deviceInfo = deviceList[0];
        devicefounded = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (devicefounded) {
            devicefounded = false;
            ConnectToDevice(deviceInfo);
        }

        if (deviceStateChanged)
        {
            deviceStateChanged = false;
            DeviceState state = device.ReadParam<DeviceState>(Parameter.State);
            if (state == DeviceState.Connected)
            {
                Debug.Log("Device connected");
                uiController.SaveDevice(device);
            }
            else
            {
                Debug.Log("Device disconnected");
                createDeviceEnumerator();
            }
            uiController.ShowMenu(state == DeviceState.Connected);
            uiController.OnDeviceStateChange(state != DeviceState.Connected);
        }

    }

    private void ConnectToDevice(DeviceInfo deviceInfo)
    {
        deviceEnumerator.DeviceListChanged -= OnDeviceFound;
        device = deviceEnumerator.CreateDevice(deviceInfo);
        device.ParameterChanged += OnDeviceParamChanged;
        device.Connect();
    }

    private void OnDeviceParamChanged(object sender, Parameter param)
    {
        if (param == Parameter.State) {
            deviceStateChanged = true;
        }
    }
    
    private void OnDestroy()
    {
        device.Disconnect();
    }

#if UNITY_ANDROID
    [DllImport("jnihelper-lib")]
    private static extern IntPtr GetJniEnv();
#endif
}
