﻿using System;
using System.Collections;
using System.Collections.Generic;
using Neuro;
using Neuro.Native;
using UnityEngine;

public class SignalController
{
    private SignalChannel signalChannel = null;

    public Action<double[]> onSignalRecieved;

    int offset = 0;
    int windowDuration = 5;
    public int plotSize = 0;

    public SignalController(Device device)
    {
        configureDevice(device);

        if (DeviceTraits.HasChannelsWithType(device, ChannelType.Signal))
        {
            var channelInfo =
                DeviceTraits.GetChannelsWithType(device, ChannelType.Signal)[0];
            signalChannel = new SignalChannel(device, channelInfo);
            plotSize = (int)Mathf.Ceil(signalChannel.SamplingFrequency * windowDuration);
            signalChannel.LengthChanged += OnSignalChanged;
            device.Execute(Command.StartSignal);
        }
    }

    private void OnSignalChanged(object sender, int length)
    {
        int totalLength = signalChannel.TotalLength;
        int readLength = totalLength - offset;
        double[] signalSamples = signalChannel.ReadData(offset, readLength);
        offset += readLength;

        onSignalRecieved?.Invoke(signalSamples);
    }

    public void CloseChannel(Device device)
    {
        device.Execute(Command.StopSignal);
        signalChannel.LengthChanged -= OnSignalChanged;
        signalChannel.Dispose();
        signalChannel = null;
        onSignalRecieved = null;
    }

    private void configureDevice(Device device)
    {
        device.SetParam(Parameter.Gain, Gain.Gain6);
        device.SetParam(Parameter.Offset, (byte)3);
        device.SetParam(Parameter.ADCInputState, ADCInput.Resistance);
        device.SetParam(Parameter.SamplingFrequency, SamplingFrequency.Hz125);
        device.SetParam(Parameter.HardwareFilterState, true);
        device.SetParam(Parameter.ExternalSwitchState, ExternalSwitchInput.MioElectrodes);
    }
}
