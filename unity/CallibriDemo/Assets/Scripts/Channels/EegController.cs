﻿using System;
using System.Collections;
using System.Collections.Generic;
using Neuro;
using Neuro.Native;
using UnityEngine;

public class EegController
{
    public Action<EegIndexValues> onIndexesChanged;
    public Action<double[]> onEegChanged;

    EegChannel eegChanel = null;
    SpectrumChannel spectrumChannel = null;
    SpectrumPowerChannel spectrumAlphaChannel = null;
    SpectrumPowerChannel spectrumBetaChannel = null;
    SpectrumPowerChannel spectrumThetaChannel = null;
    SpectrumPowerChannel spectrumDeltaChannel = null;

    EegIndexValues indexValues = new EegIndexValues();

    int offset = 0;
    int windowDuration = 5;
    public int plotSize = 0;

    public EegController(Device device)
    {
        configureDevice(device);

        if (DeviceTraits.HasChannelsWithType(device, ChannelType.Signal))
        {
            var channelInfo =
                DeviceTraits.GetChannelsWithType(device, ChannelType.Signal)[0];
            eegChanel = new EegChannel(device, channelInfo);
            spectrumChannel = new SpectrumChannel(eegChanel);
            spectrumAlphaChannel = new SpectrumPowerChannel(new SpectrumChannel[] { spectrumChannel }, 8.0f, 14.0f, "alpha");
            spectrumAlphaChannel.LengthChanged += onSpectrumAlphaLengthChanged;
            spectrumBetaChannel = new SpectrumPowerChannel(new SpectrumChannel[] { spectrumChannel }, 14.0f, 24.0f, "beta");
            spectrumBetaChannel.LengthChanged += onSpectrumBetaLengthChanged;
            spectrumThetaChannel = new SpectrumPowerChannel(new SpectrumChannel[] { spectrumChannel }, 4.0f, 8.0f, "theta");
            spectrumThetaChannel.LengthChanged += onSpectrumThetaLengthChanged;
            spectrumDeltaChannel = new SpectrumPowerChannel(new SpectrumChannel[] { spectrumChannel }, 0.5f, 4.0f, "delta");
            spectrumDeltaChannel.LengthChanged += onSpectrumDeltaLengthChanged;

            plotSize = (int)Mathf.Ceil(eegChanel.SamplingFrequency * windowDuration);
            eegChanel.LengthChanged += OnEegChannelLengthChanged;
            device.Execute(Command.StartSignal);
        }
    }

    private void onSpectrumDeltaLengthChanged(object sender, int length)
    {
        double delta = spectrumDeltaChannel.ReadData(spectrumDeltaChannel.TotalLength - 1, 1)[0];
        indexValues.DeltaRate = delta;
        onIndexesChanged?.Invoke(indexValues);
    }

    private void onSpectrumThetaLengthChanged(object sender, int length)
    {
        double theta = spectrumThetaChannel.ReadData(spectrumThetaChannel.TotalLength - 1, 1)[0];
        indexValues.ThetaRate = theta;
        onIndexesChanged?.Invoke(indexValues);
    }

    private void onSpectrumBetaLengthChanged(object sender, int length)
    {
        double beta = spectrumBetaChannel.ReadData(spectrumBetaChannel.TotalLength - 1, 1)[0];
        indexValues.BetaRate = beta;
        onIndexesChanged?.Invoke(indexValues);
    }

    private void onSpectrumAlphaLengthChanged(object sender, int length)
    {
        double alpha = spectrumAlphaChannel.ReadData(spectrumAlphaChannel.TotalLength - 1, 1)[0];
        indexValues.AlphaRate = alpha;
        onIndexesChanged?.Invoke(indexValues);
    }

    private void OnEegChannelLengthChanged(object sender, int length)
    {
        int totalLength = eegChanel.TotalLength;
        int readLength = totalLength - offset;
        double[] signalSamples = eegChanel.ReadData(offset, readLength);
        offset += readLength;
        onEegChanged?.Invoke(signalSamples);
    }

    private void configureDevice(Device device)
    {
        device.SetParam(Parameter.Gain, Gain.Gain6);
        device.SetParam(Parameter.Offset, (byte)3);
        device.SetParam(Parameter.ADCInputState, ADCInput.Resistance);
        device.SetParam(Parameter.SamplingFrequency, SamplingFrequency.Hz250);
        device.SetParam(Parameter.HardwareFilterState, true);
        device.SetParam(Parameter.ExternalSwitchState, ExternalSwitchInput.MioElectrodes);
    }

    public void CloseChannel(Device device)
    {
        device.Execute(Command.StopSignal);
        spectrumAlphaChannel.LengthChanged -= onSpectrumAlphaLengthChanged;
        spectrumAlphaChannel.Dispose();
        spectrumAlphaChannel = null;
        spectrumBetaChannel.LengthChanged -= onSpectrumBetaLengthChanged;
        spectrumBetaChannel.Dispose();
        spectrumBetaChannel = null;
        spectrumThetaChannel.LengthChanged -= onSpectrumThetaLengthChanged;
        spectrumThetaChannel.Dispose();
        spectrumThetaChannel = null;
        spectrumDeltaChannel.LengthChanged -= onSpectrumDeltaLengthChanged;
        spectrumDeltaChannel.Dispose();
        spectrumDeltaChannel = null;

        spectrumChannel.Dispose();
        spectrumChannel = null;

        eegChanel.LengthChanged -= OnEegChannelLengthChanged;
        eegChanel.Dispose();
        eegChanel = null;

        onIndexesChanged = null;
        onEegChanged = null;
    }

}
