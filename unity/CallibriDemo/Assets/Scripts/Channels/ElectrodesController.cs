﻿using Neuro;
using Neuro.Native;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectrodesController
{
    public enum ElectrodeState
    {
        Lost,
        Normal
    }

    public Action<ElectrodeState> onElChanged;

    public ElectrodesController(Device device)
    {
        if (DeviceTraits.HasChannelsWithType(device, ChannelType.ElectrodesState))
        {
            var electrodesChannelInfo =
                DeviceTraits.GetChannelsWithType(device, ChannelType.ElectrodesState)[0];
            device.AddSignalChannelDataListener(onElectrodeStateChanged, electrodesChannelInfo);
            device.Execute(Command.StartSignal);
        }
    }
    
    private void onElectrodeStateChanged(object device, Device.SignalChannelData data)
    {
        float state = Mathf.Abs((float)(data.DataArray[0] * 1e6));
        if (state < 1)
        {
            onElChanged?.Invoke(ElectrodeState.Lost);
        }
        else
        {
            onElChanged?.Invoke(ElectrodeState.Normal);
        }
    }

    public void CloseChannel(Device device)
    {
        device.Execute(Command.StopSignal);
        device.RemoveSignalChannelDataListener(onElectrodeStateChanged);
        onElChanged = null;
    }

}
