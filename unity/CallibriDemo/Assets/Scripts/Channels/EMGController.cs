﻿using System;
using System.Collections;
using System.Collections.Generic;
using Neuro;
using Neuro.Native;
using UnityEngine;

public class EMGController
{
    private SignalChannel signalChannel = null;
    private ElectrodeStateChannel electrodeStateChannel = null;

    private RPeakChannel rPeakChannel = null;
    private HeartRateChannel hrChannel = null;
    private StressIndexChannel siChannel = null;

    public Action<int[]> onHrRecieved;
    public Action<double[]> onSiRecieved;

    int windowDuration = 5;
    int HROffset = 0;
    public int HRPlotSize = 0;
    int SIOffset = 0;
    public int SIPlotSize = 0;

    public EMGController(Device device)
    {
        configureDevice(device);

        electrodeStateChannel = new ElectrodeStateChannel(device);

        if (DeviceTraits.HasChannelsWithType(device, ChannelType.Signal))
        {
            var channelInfo =
                DeviceTraits.GetChannelsWithType(device, ChannelType.Signal)[0];

            signalChannel = new SignalChannel(device, channelInfo);
            rPeakChannel = new RPeakChannel(signalChannel, electrodeStateChannel);

            hrChannel = new HeartRateChannel(rPeakChannel);
            HRPlotSize = (int)Mathf.Ceil(hrChannel.SamplingFrequency * windowDuration);
            hrChannel.LengthChanged += onHRRecieved;

            siChannel = new StressIndexChannel(rPeakChannel);
            SIPlotSize = (int)Mathf.Ceil(siChannel.SamplingFrequency * windowDuration);
            siChannel.LengthChanged += onSIRecieved;

            device.Execute(Command.StartSignal);
        }
    
    }

    private void onSIRecieved(object sender, int e)
    {
        int totalLength = siChannel.TotalLength;
        int readLength = totalLength - SIOffset;
        double[] siSample = siChannel.ReadData(SIOffset, readLength);
        SIOffset += readLength;
        //double[] siSample = siChannel.ReadData(siChannel.TotalLength - 1, 1);
        onSiRecieved?.Invoke(siSample);
    }

    private void onHRRecieved(object sender, int e)
    {
        int totalLength = hrChannel.TotalLength;
        int readLength = totalLength - HROffset;
        int[] hrSample = hrChannel.ReadData(HROffset, readLength);
        HROffset += readLength;
        onHrRecieved?.Invoke(hrSample);
    }

    public void CloseChannels(Device device)
    {
        device.Execute(Command.StopSignal);

        siChannel.LengthChanged -= onSIRecieved;
        siChannel.Dispose();
        siChannel = null;

        hrChannel.LengthChanged -= onHRRecieved;
        hrChannel.Dispose();
        hrChannel = null;

        rPeakChannel.Dispose();
        rPeakChannel = null;

        signalChannel.Dispose();
        signalChannel = null;

        electrodeStateChannel.Dispose();
        electrodeStateChannel = null;

        onHrRecieved = null;
        onSiRecieved = null;
    }

    private void configureDevice(Device device)
    {
        device.SetParam(Parameter.Gain, Gain.Gain12);
        device.SetParam(Parameter.Offset, (byte)4);
        device.SetParam(Parameter.ADCInputState, ADCInput.Resistance);
        device.SetParam(Parameter.SamplingFrequency, SamplingFrequency.Hz125);
        device.SetParam(Parameter.ExternalSwitchState, ExternalSwitchInput.MioElectrodes);
    }
}
