1. В основной проект добавить .NET библиотеку neurosdk.dll
2. Добавляем собственную ссылку в iOS-таргете на neurosdk.framework.
3. В настройках iOS-таргета указать дополнительные аргументы mtouch:
	-gcc_flags="-framework neurosdk"
4. В Info.plist добавить свойства NSBluetoothPeripheralUsageDescription, NSBluetoothAlwaysUsageDescription
