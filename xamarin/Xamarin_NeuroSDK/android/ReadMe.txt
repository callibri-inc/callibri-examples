Рекомендуемый вариант:

1. В основной проект добавить .NET библиотеку neurosdk.dll
2. Папки jars и lib скопировать в Android-таргет
3. Файлам из папки jar установить действие при сборке AndroidJavaLibrary
4. Файлам из папки lib установить действие при сборке AndroidNativeLibrary
5. В MainActivity подключаем добавленные библиотеки:

	перед LoadApplication(new App()); добавляем строки:

	    JavaSystem.LoadLibrary("c++_shared");
            JavaSystem.LoadLibrary("neurosdk");
            JavaSystem.LoadLibrary("android-neurosdk");

	Для поиска устройств необходимы ссылки на jniEnv и контекст приложения. Их можно получить 
в Android-таргете, как:
IntPtr jniEnv = JNIEnv.Handle;
IntPtr context = JNIEnv.ToJniHandle(Android.App.Application.Context);
6. В манифест добавить разрешения ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION, ACCESS_BACKGROUND_LOCATION, BLUETOOTH, BLUETOOTH_ADMIN

Альтернативный вариант:

1. В основной проект добавить .NET библиотеку neurosdk.dll
2. Создать библиотеку привязок для neurosdk.aar (https://docs.microsoft.com/ru-ru/xamarin/android/platform/binding-java-library/binding-an-aar)
3. Добавить созданную библиотеку в Android-таргет
