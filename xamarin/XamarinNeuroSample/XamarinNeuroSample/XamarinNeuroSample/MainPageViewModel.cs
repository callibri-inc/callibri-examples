﻿using Neuro;
using Neuro.Native;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using XamarinNeuroSample.Utils;

namespace XamarinNeuroSample
{
    public class NDeviceInfo 
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string SerialNumber { get; set; }
    }

    class MainPageViewModel : INotifyPropertyChanged
    {

        public ObservableCollection<NDeviceInfo> FoundDevices { get; set; }

        public ICommand StartSearchCommand { private set; get; }
        public ICommand ConnectToDevice { private set; get; }

        private string _deviceInfo = "";
        public string DeviceInfo 
        {
            set { SetProperty(ref _deviceInfo, value); }
            get { return _deviceInfo; }
        }

        private string _connectionState = "Connected";
        public string ConnectionState 
        {
            set { SetProperty(ref _connectionState, value); }
            get { return _connectionState; }
        }

        private int _batteryValue = 0;
        public int BatteryValue
        {
            set { SetProperty(ref _batteryValue, value); }
            get { return _batteryValue; }
        }

        private bool _hasDevice = false;
        public bool HasDevice
        {
            set { SetProperty(ref _hasDevice, value); }
            get { return _hasDevice; }
        }

        private IntPtr jniEnv;
        private IntPtr context;

        public MainPageViewModel() 
        {
            StartSearchCommand = new Xamarin.Forms.Command(
                       execute: () => {
                           Close();
                           StartSearch();
                       }
                       );

            ConnectToDevice = new Xamarin.Forms.Command<NDeviceInfo>(
                        execute: (NDeviceInfo info) => {
                            if (AvailableConnect(info))
                            {
                                Connect(info);
                            }
                            else 
                            {
                                ISensorHelper sensorHelper = DependencyService.Get<ISensorHelper>();
                                sensorHelper.BoundTo(info.Address, (bounded) =>
                                {
                                    if (bounded)
                                    {
                                        Connect(info);
                                    }
                                });
                            }
                        }
                    );
        }

        public void Init(IntPtr jniEnv, IntPtr context) 
        {
            this.jniEnv = jniEnv;
            this.context = context;
        }

        #region SearchDevice

        private DeviceEnumerator currentEnumerator = null;

        private void StartSearch()
        {
            ISensorHelper sensorHelper = DependencyService.Get<ISensorHelper>();
            sensorHelper.EnableSensor((enabled) =>
            {
                if (enabled)
                {
                    CreateEnumerator();
                }
            });
        }

        /// <summary>
        /// Creating DeviceEnumerator for search devices
        /// </summary>
        private void CreateEnumerator()
        {
            DeviceType deviceType = DeviceType.Any;

            FoundDevices = new ObservableCollection<NDeviceInfo>();

            switch (Xamarin.Forms.Device.RuntimePlatform)
            {
                case Xamarin.Forms.Device.Android:
                    currentEnumerator = new DeviceEnumerator(deviceType, jniEnv, context);
                    Console.WriteLine("Android enumerator created!");
                    break;
                case Xamarin.Forms.Device.iOS:
                    currentEnumerator = new DeviceEnumerator(deviceType);
                    Console.WriteLine("iOS enumerator created!");
                    break;
                case Xamarin.Forms.Device.UWP:
                default:
                    Console.WriteLine("Wrong platform!");
                    break;
            }

            if (currentEnumerator != null)
            {
                currentEnumerator.DeviceListChanged += onDeviceFound;
            }

        }

        /// <summary>
        /// Callback for any found device. Each new device will be added to list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onDeviceFound(object sender, EventArgs e)
        {
            foreach (DeviceInfo info in currentEnumerator.Devices)
            {
                NDeviceInfo nDeviceInfo = new NDeviceInfo() { Name = info.Name, Address = info.Id, SerialNumber = info.SerialNumber.ToString() };
                if (!FoundDevices.Any(i => i.Address == nDeviceInfo.Address))
                {
                    FoundDevices.Add(nDeviceInfo);
                }
            }
            OnPropertyChanged(nameof(FoundDevices));
        }

        /// <summary>
        /// Stop searching by remove enumerator
        /// </summary>
        protected void CloseEnumerator()
        {
            FoundDevices?.Clear();

            var enumerator = currentEnumerator;
            currentEnumerator = null;
            if (enumerator != null)
            {
                enumerator.DeviceListChanged -= onDeviceFound;
                enumerator.Dispose();
            }

            Console.WriteLine("[Device] Close enumerator");
        }

        #endregion

        #region Device actions

        private Neuro.Device device = null;


        private bool AvailableConnect(NDeviceInfo info) 
        {
            string currentDeviceName = info.Name.ToLower();
            if (currentDeviceName.Contains("black"))
            {
                ISensorHelper sensorHelper = DependencyService.Get<ISensorHelper>();
                return sensorHelper.isDeviceBounded(info.Address);
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Creating and connect to device by it info
        /// </summary>
        /// <param name="info">name, address and serial number</param>
        private void Connect(NDeviceInfo info)
        {
            CloseDevice();

            try
            {
                if (currentEnumerator == null)
                {
                    CreateEnumerator();
                }

                device = currentEnumerator.CreateDevice(new Neuro.Native.DeviceInfo()
                {
                    Name = info.Name,
                    Id = info.Address,
                    SerialNumber = Convert.ToUInt64(info.SerialNumber)
                });
                device.ParameterChanged += onParameterChanged;
                device.Connect();

                //create battery
                CreateBatteryChannel();
            }
            catch (Exception ex)
            {
                ConnectionState = "Disconnected";
                Console.WriteLine("Connection exception: " + ex.Message);
            }
            finally
            {
                try
                {
                    CloseEnumerator();
                }
                catch (System.Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        /// <summary>
        /// Callback for changing device state
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="param"></param>
        private void onParameterChanged(object sender, Parameter param)
        {
            var device = this.device;

            if (device != null)
            {
                if (param == Parameter.State)
                {
                    var deviceState = device.ReadParam<Neuro.Native.DeviceState>(Parameter.State);
                    ConnectionState = deviceState.ToString();
                    switch (deviceState)
                    {
                        case DeviceState.Connected:
                            HasDevice = true;
                            GetDeviceInfo();
                            break;
                        case DeviceState.Disconnected:
                            HasDevice = false;
                            break;
                    }
                }
            }
        }

        private void GetDeviceInfo()
        {
            string info = "";
            info += "*Common params*\n";
            info += string.Format("Name: [{0}]\n", device.ReadParam<string>(Neuro.Native.Parameter.Name));
            info += string.Format("Address: [{0}]\n", device.ReadParam<string>(Neuro.Native.Parameter.Address));
            info += string.Format("Serial Number: [{0}]\n", device.ReadParam<string>(Neuro.Native.Parameter.SerialNumber));

            FirmwareVersion fv = device.ReadParam<FirmwareVersion>(Neuro.Native.Parameter.FirmwareVersion);
            info += string.Format("Version: [{0}.{1}]\n", fv.Version, fv.Build);

            FirmwareMode fm = device.ReadParam<FirmwareMode>(Neuro.Native.Parameter.FirmwareMode);
            info += string.Format("Mode: [{0}]\n", fm.ToString());

            info += "*Supported params*\n";
            foreach (ParamInfo paramInfo in device.Parameters)
            {
                info += string.Format("Name: [{0}] Type: [{1}] Access: [{2}]\n",
                    paramInfo.Parameter.ToString(), paramInfo.Parameter.GetType(), paramInfo.Access);
            }

            info += "*Supported device channels*\n";
            foreach (ChannelInfo channelInfo in device.Channels)
            {
                info += string.Format("Name: [{0}] Type: [{1}] Index: [{2}]\n",
                    channelInfo.Name, channelInfo.Type, channelInfo.Index);
            }

            info += "*Supported commands*\n";
            foreach (Neuro.Native.Command command in device.Commands)
            {
                info += string.Format("{0}\n", command);
            }
            DeviceInfo = info;
        }

        /// <summary>
        /// Disconnect and remove device
        /// </summary>
        private void CloseDevice()
        {
            var device = this.device;

            this.device = null;
            if (device != null)
            {
                device.ParameterChanged -= onParameterChanged;
                device.Disconnect();
                device.Dispose();
                HasDevice = false;
            }
        }
        #endregion

        #region Battery

        private BatteryChannel batteryChannel;

        /// <summary>
        /// Create battery channel
        /// </summary>
        private void CreateBatteryChannel()
        {
            batteryChannel = new BatteryChannel(device);
            batteryChannel.LengthChanged += OnChannelLengthChanged;
        }

        public void OnChannelLengthChanged(object sender, int length)
        {
            BatteryValue = batteryChannel.ReadData(batteryChannel.TotalLength - 1, 1)[0];
        }

        private void RemoveBatteryChannel() 
        {
            batteryChannel.LengthChanged -= OnChannelLengthChanged;
            batteryChannel.Dispose();
            batteryChannel = null;
        }

        #endregion

        /// <summary>
        /// Remove all
        /// </summary>
        private void Close()
        {
            try
            {
                RemoveBatteryChannel();
                CloseDevice();
                CloseEnumerator();
            }
            catch (System.Exception)
            {
            }
        }

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null)
        {
            if (Object.Equals(storage, value))
                return false;

            storage = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        private void OnPropertyChanged(string name) 
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion
    }
}
