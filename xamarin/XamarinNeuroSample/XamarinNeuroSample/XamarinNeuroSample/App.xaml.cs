﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinNeuroSample
{
    public partial class App : Application
    {
        public App(IntPtr jniEnv, IntPtr context)
        {
            InitializeComponent();

            MainPage page = new MainPage();
            if (Device.RuntimePlatform == Device.Android)
            {
                page.SetAndroidEnviroment(jniEnv, context);
            }

            MainPage = page;
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
