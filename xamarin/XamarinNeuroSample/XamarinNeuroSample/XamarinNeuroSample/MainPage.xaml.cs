﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamarinNeuroSample
{
    public partial class MainPage : ContentPage
    {

        private MainPageViewModel viewModel = null;
        public MainPage()
        {
            InitializeComponent();

            viewModel = new MainPageViewModel();

            BindingContext = viewModel;
        }

        public void SetAndroidEnviroment(IntPtr jniEnv, IntPtr context) 
        {
            viewModel.Init(jniEnv, context);
        }
    }
}
