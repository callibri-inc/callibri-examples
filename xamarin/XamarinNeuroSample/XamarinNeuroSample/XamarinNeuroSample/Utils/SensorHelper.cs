﻿using System;

namespace XamarinNeuroSample.Utils
{
    public interface ISensorHelper
    {
        bool isDeviceBounded(string adr);
        void BoundTo(string adr, Action<bool> bounded);
        void EnableSensor(Action<bool> enabled);
    }
}
