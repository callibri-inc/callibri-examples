﻿using System;
using Xamarin.Forms;
using XamarinNeuroSample.iOS.Utils;
using XamarinNeuroSample.Utils;

[assembly: Dependency(typeof(SensorHelper))]
namespace XamarinNeuroSample.iOS.Utils
{
    public class SensorHelper : ISensorHelper
    {
        public void BoundTo(string adr, Action<bool> bounded)
        {
            bounded?.Invoke(true);
        }

        public void EnableSensor(Action<bool> enabled)
        {
            enabled?.Invoke(true);
        }

        public bool isDeviceBounded(string adr)
        {
            return true;
        }
    }
}
