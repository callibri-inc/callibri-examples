﻿using Android;
using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.Content.PM;
using Android.Gms.Common.Apis;
using Android.Gms.Location;
using Android.Locations;
using Android.OS;
using Android.Util;
using AndroidX.Core.App;
using AndroidX.Core.Content;
using Java.Util.Concurrent.Atomic;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Android.Gms.Common;
using XamarinNeuroSample.Droid.Utils;
using XamarinNeuroSample.Utils;

[assembly: Dependency(typeof(SensorHelper))]
namespace XamarinNeuroSample.Droid.Utils
{
    public class SensorHelper : ISensorHelper
    {
        private static BroadcastReceiver _receiverBound;
        private static Action<bool> boundedAction;

        private readonly int REQUEST_PERMISSIONS = 222;

        private static AtomicBoolean ready = new AtomicBoolean(false);
        private static Action<bool> sensorReadyAction;

        public bool isDeviceBounded(string adr)
        {
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.DefaultAdapter;
            ICollection<BluetoothDevice> boundedDevices = bluetoothAdapter.BondedDevices;
            bool ready = false;
            foreach (BluetoothDevice it in boundedDevices)
            {
                if (it.Address.Equals(adr))
                {
                    ready = true;
                    break;
                }
            }
            return ready;
        }

        public void BoundTo(string adr, Action<bool> bounded)
        {
            boundedAction = bounded;
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.DefaultAdapter;
            BluetoothDevice dev = bluetoothAdapter.GetRemoteDevice(adr);
            if (dev != null)
            {
                _receiverBound = new BTReceiver();

                Android.App.Application.Context.RegisterReceiver(_receiverBound, new IntentFilter(BluetoothDevice.ActionBondStateChanged));
                dev.CreateBond();
            }

        }

        [BroadcastReceiver(Enabled = true, Exported = true)]
        [IntentFilter(new[] { BluetoothDevice.ActionBondStateChanged })]
        public class BTReceiver : BroadcastReceiver
        {
            public override void OnReceive(Context context, Intent intent)
            {
                if (intent != null)
                {
                    Bundle extras = intent.Extras;
                    if (extras != null)
                    {
                        int bondState = extras.GetInt(BluetoothDevice.ExtraBondState);
                        if (bondState == 12/*BluetoothDevice.BOND_BONDED*/)
                        {
                            boundedAction?.Invoke(true);
                            try
                            {
                                Android.App.Application.Context.UnregisterReceiver(_receiverBound);
                            }
                            catch(Exception ex) 
                            {
                                Log.Debug("SensorHelper", ex.Message);
                            }
                        }
                        else if (bondState == 10)
                        {
                            boundedAction?.Invoke(false);
                            try
                            {
                                Android.App.Application.Context.UnregisterReceiver(_receiverBound);
                            }
                            catch (Exception ex) 
                            {
                                Log.Debug("SensorHelper", ex.Message);
                            }
                        }
                    }
                }


            }
        }

        public void EnableSensor(Action<bool> enabled)
        {
            sensorReadyAction = enabled;
            requestPermissions();
        }

        private static void invokeSensorsReady()
        {
            sensorReadyAction?.Invoke(ready.Get());
        }

        private void requestPermissions()
        {
            if (ContextCompat.CheckSelfPermission(Android.App.Application.Context, Manifest.Permission.Bluetooth) == (int)Permission.Granted &&
                ContextCompat.CheckSelfPermission(Android.App.Application.Context, Manifest.Permission.AccessFineLocation) == (int)Permission.Granted &&
                ContextCompat.CheckSelfPermission(Android.App.Application.Context, Manifest.Permission.AccessCoarseLocation) == (int)Permission.Granted)
            {
                turnOnBT();
            }
            else
            {
                MessagingCenter.Subscribe<MainActivity, bool>(this, "RequestPermissions", (sender, granted) =>
                {
                    if (granted)
                    {
                        turnOnBT();
                    }
                    else
                    {
                        ready.Set(false);
                        invokeSensorsReady();
                    }
                    MessagingCenter.Unsubscribe<MainActivity, bool>(this, "RequestPermissions");
                });
                String[] permissions = new String[] { Manifest.Permission.Bluetooth, Manifest.Permission.AccessFineLocation, Manifest.Permission.AccessCoarseLocation };
                ActivityCompat.RequestPermissions((Activity)MainActivity.ActivityContext, permissions, REQUEST_PERMISSIONS);
            }
        }

        private void turnOnBT()
        {
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.DefaultAdapter;
            if (!bluetoothAdapter.IsEnabled)
            {
                MessagingCenter.Subscribe<MainActivity, bool>(this, "EnableBT", (sender, granted) =>
                {
                    MessagingCenter.Unsubscribe<MainActivity, bool>(this, "EnableBT");
                    if (granted)
                    {
                        turnOnGps();
                    }
                    else
                    {
                        ready.Set(false);
                        invokeSensorsReady();
                    }
                });
                Intent enableBT = new Intent(BluetoothAdapter.ActionRequestEnable);
                ((MainActivity)MainActivity.ActivityContext).StartActivityForResult(enableBT, 555);


            }
            else
            {
                turnOnGps();
            }

        }

        public async void turnOnGps()
        {
            try
            {
                LocationManager locationManager = (LocationManager)Android.App.Application.Context.GetSystemService(Context.LocationService);
                LocationProvider gpsProvider = locationManager.GetProvider(LocationManager.GpsProvider);

                if (!locationManager.IsProviderEnabled(gpsProvider.Name))
                {
                    if (isGooglePlayServicesAvailable(MainActivity.ActivityContext))
                    {
                        GoogleApiClient googleApiClient = new GoogleApiClient.Builder((MainActivity)MainActivity.ActivityContext).AddApi(LocationServices.API).Build();
                        googleApiClient.Connect();
                        LocationRequest locationRequest = LocationRequest.Create();
                        locationRequest.SetPriority(LocationRequest.PriorityLowPower);
                        locationRequest.SetInterval(5);
                        locationRequest.SetFastestInterval(5);

                        LocationSettingsRequest.Builder
                            locationSettingsRequestBuilder = new LocationSettingsRequest.Builder()
                            .AddLocationRequest(locationRequest);
                        locationSettingsRequestBuilder.SetAlwaysShow(false);

                        LocationSettingsResult locationSettingsResult = await LocationServices.SettingsApi.CheckLocationSettingsAsync(
                            googleApiClient, locationSettingsRequestBuilder.Build());

                        MessagingCenter.Subscribe<MainActivity, bool>(this, "EnableGPS", (sender, granted) =>
                        {
                            MessagingCenter.Unsubscribe<MainActivity, bool>(this, "EnableGPS");
                            if (granted)
                            {
                                ready.Set(true);
                                invokeSensorsReady();
                            }
                            else
                            {
                                ready.Set(false);
                                invokeSensorsReady();
                            }
                        });

                        if (locationSettingsResult.Status.StatusCode == LocationSettingsStatusCodes.ResolutionRequired)
                        {
                            MessagingCenter.Subscribe<MainActivity, bool>(this, "EnableGPS", (sender, granted) =>
                            {
                                MessagingCenter.Unsubscribe<MainActivity, bool>(this, "EnableGPS");
                                if (granted)
                                {
                                    ready.Set(true);
                                    invokeSensorsReady();
                                }
                                else
                                {
                                    ready.Set(false);
                                    invokeSensorsReady();
                                }
                            });
                            locationSettingsResult.Status.StartResolutionForResult((MainActivity)MainActivity.ActivityContext, 666);
                        }
                        else
                        {
                            ready.Set(true);
                            invokeSensorsReady();
                        }
                    }
                    else
                    {
                        Intent intent = new Intent(Android.Provider.Settings.ActionLocationSourceSettings);
                        ((MainActivity)MainActivity.ActivityContext).StartActivity(intent);
                    }
                }
                else
                {
                    ready.Set(true);
                    invokeSensorsReady();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

            }
        }

        public bool isGooglePlayServicesAvailable(Context context)
        {
            GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.Instance;
            int resultCode = googleApiAvailability.IsGooglePlayServicesAvailable(context);
            return resultCode == ConnectionResult.Success;
        }
    }
}