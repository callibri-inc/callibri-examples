package com.neurosdk.callibri.demo;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.androidplot.xy.XYPlot;
import com.neuromd.neurosdk.ADCInput;
import com.neuromd.neurosdk.ChannelInfo;
import com.neuromd.neurosdk.ChannelType;
import com.neuromd.neurosdk.Command;
import com.neuromd.neurosdk.Device;
import com.neuromd.neurosdk.DeviceState;
import com.neuromd.neurosdk.EegChannel;
import com.neuromd.neurosdk.ExternalSwitchInput;
import com.neuromd.neurosdk.Gain;
import com.neuromd.neurosdk.Parameter;
import com.neuromd.neurosdk.ParameterName;
import com.neuromd.neurosdk.SamplingFrequency;
import com.neuromd.neurosdk.SpectrumChannel;
import com.neuromd.neurosdk.SpectrumPowerChannel;
import com.neurosdk.callibri.example.utils.CommonHelper;
import com.neurosdk.callibri.example.utils.PlotHolder;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EEGDemoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EEGDemoFragment extends Fragment implements ICallibriFragment {
    private final String TAG = "[SignalDemo]";
    private PlotHolder plot;
    private Future<?> _futureUpd;
    private SpectrumPowerChannel _specAlphaChannel;
    private SpectrumPowerChannel _specBetaChannel;
    private SpectrumPowerChannel _specDeltaChannel;
    private SpectrumPowerChannel _specThetaChannel;


    public EEGDemoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment
     *
     * @return A new instance of fragment DemoModeFragment.
     */
    public static EEGDemoFragment newInstance() {
        return new EEGDemoFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return initPlot(inflater.inflate(R.layout.fragment_eeg_demo, container, false));
    }

    private View initPlot(View rootView) {
        plot = new PlotHolder((XYPlot) rootView.findViewById(R.id.plot_signal));
        rootView.findViewById(R.id.btn_zoom_in).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plot.zoomYIn();
            }
        });
        rootView.findViewById(R.id.btn_zoom_out).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plot.zoomYOut();
            }
        });
        return rootView;
    }

    private double getIndexValue(SpectrumPowerChannel channel) {
        if (channel != null) {
            int ttLen = channel.totalLength();
            if (ttLen > 0) {
                double[] vals = channel.readData(ttLen - 1, 1);
                if (vals != null && vals.length > 0)
                    return vals[0];
            }
        }
        return 0.0;
    }

    private void updateData() {
        final double alpha = getIndexValue(_specAlphaChannel);
        final double beta = getIndexValue(_specBetaChannel);
        final double delta = getIndexValue(_specDeltaChannel);
        final double theta = getIndexValue(_specThetaChannel);

        FragmentActivity activity = getActivity();
        if (activity != null && isAdded()) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    View view = getView();
                    if (view != null && isAdded()) {
                        TextView txtAlphaIndexValue = view.findViewById(R.id.txt_alpha_index_value);
                        TextView txtBetaIndexValue = view.findViewById(R.id.txt_beta_index_value);
                        TextView txtDeltaIndexValue = view.findViewById(R.id.txt_delta_index_value);
                        TextView txtThetaIndexValue = view.findViewById(R.id.txt_theta_index_value);
                        if (txtAlphaIndexValue != null)
                            txtAlphaIndexValue.setText(getString(R.string.eeg_index_value, alpha));
                        if (txtBetaIndexValue != null)
                            txtBetaIndexValue.setText(getString(R.string.eeg_index_value, beta));
                        if (txtDeltaIndexValue != null)
                            txtDeltaIndexValue.setText(getString(R.string.eeg_index_value, delta));
                        if (txtThetaIndexValue != null)
                            txtThetaIndexValue.setText(getString(R.string.eeg_index_value, theta));
                    }
                }
            });
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            Device device = DevHolder.inst().device();
            if (device != null) {
                ChannelInfo channelInfo = DevHolder.inst().getDevChannel(ChannelType.Signal);
                if (channelInfo != null) {
                    configureDevice(device);
                    initChannels(device, channelInfo);

                    _futureUpd = Executors.newFixedThreadPool(1).submit(new Runnable() {
                        @SuppressWarnings("BusyWait")
                        @Override
                        public void run() {
                            try {
                                while (!Thread.currentThread().isInterrupted()) {
                                    Thread.sleep(500);
                                    updateData();
                                }
                            } catch (Exception ignored) {
                            }
                        }
                    });
                }
            }
        } catch (Exception ex) {
            Log.d(TAG, "Failed start signal", ex);
            CommonHelper.showMessage(this, R.string.err_start_signal);
        }
    }

    private void initChannels(Device device, ChannelInfo channelInfo) {
        EegChannel eegChannel = new EegChannel(device, channelInfo);
        SpectrumChannel spectrumChannel = new SpectrumChannel(eegChannel);
        _specAlphaChannel = new SpectrumPowerChannel(new SpectrumChannel[]{spectrumChannel}, 8.0f, 14.0f, "alpha");
        _specBetaChannel = new SpectrumPowerChannel(new SpectrumChannel[]{spectrumChannel}, 14.0f, 24.0f, "beta");
        _specDeltaChannel = new SpectrumPowerChannel(new SpectrumChannel[]{spectrumChannel}, 0.5f, 4.0f, "delta");
        _specThetaChannel = new SpectrumPowerChannel(new SpectrumChannel[]{spectrumChannel}, 4.0f, 8.0f, "theta");

        plot.startRender(eegChannel, PlotHolder.ZoomVal.V_001, 5.0f);
    }

    private void configureDevice(Device device) {
        Parameter[] params = device.parameters();
        for (Parameter it : params) {
            switch (it.getName()) {
                case Gain:
                    if (device.readParam(ParameterName.Gain) != Gain.Gain6)
                        device.setParam(ParameterName.Gain, Gain.Gain6);
                    break;
                case Offset:
                    if ((byte) device.readParam(ParameterName.Offset) != 3)
                        device.setParam(ParameterName.Offset, (byte) 3);
                    break;
                case ADCInputState:
                    if (device.readParam(ParameterName.ADCInputState) != ADCInput.Resistance)
                        device.setParam(ParameterName.ADCInputState, ADCInput.Resistance);
                    break;
                case SamplingFrequency:
                    if (device.readParam(ParameterName.SamplingFrequency) != SamplingFrequency.Hz250)
                        device.setParam(ParameterName.SamplingFrequency, SamplingFrequency.Hz250);
                    break;
                case HardwareFilterState:
                    if (!(boolean) device.readParam(ParameterName.HardwareFilterState))
                        device.setParam(ParameterName.HardwareFilterState, true);
                    break;
                case ExternalSwitchState:
                    if (device.readParam(ParameterName.ExternalSwitchState) != ExternalSwitchInput.MioElectrodes)
                        device.setParam(ParameterName.ExternalSwitchState, ExternalSwitchInput.MioElectrodes);
                    break;
            }
        }
        device.execute(Command.StartSignal);
    }

    @Override
    public void stopProcess() {
        if (plot != null) {
            plot.stopRender();
            try {
                Future<?> futureUpd = _futureUpd;
                if (futureUpd != null) {
                    _futureUpd = null;
                    futureUpd.cancel(true);
                }

                Device device = DevHolder.inst().device();
                if (device != null) {
                    if (device.readParam(ParameterName.State) == DeviceState.Connected) {
                        device.execute(Command.StopSignal);
                    }
                }
            } catch (Exception ex) {
                Log.d(TAG, "Failed stop signal", ex);
                CommonHelper.showMessage(this, R.string.err_stop_signal);
            }
        }
    }
}