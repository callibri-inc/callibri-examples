package com.neurosdk.callibri.demo;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends AppCompatActivity {
    private DemoModeFragment _demoModeFragment;
    private DevSearchFragment _deDevSearchFragment;
    private DeviceInfoFragment _deviceInfoFragment;
    private SignalDemoFragment _signalDemoFragment;
    private EnvelopeDemoFragment _envelopeDemoFragment;
    private EcgDemoFragment _ecgDemoFragment;
    private EEGDemoFragment _eegDemoFragment;
    private RespirationDemoFragment _respirationDemoFragment;
    private StimulationDemoFragment _stimulationDemoFragment;
    private SpectrumDemoFragment _spectrumDemoFragment;
    private SpectrumPowerDemoFragment _spectrumPowerDemoFragment;

    private TextView txtDevState;
    private TextView txtDevBatteryPower;
    private DemoMode _demoMode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txtDevState = findViewById(R.id.txt_dev_state);
        txtDevBatteryPower = findViewById(R.id.txt_dev_battery_power);

        DevHolder.inst().init(this);
        DevHolder.inst().addCallback(new DevHolder.IDeviceHolderCallback() {
            @Override
            public void batteryChanged(int val) {
                txtDevBatteryPower.setText(getString(R.string.dev_power_prc, val));
            }

            @Override
            public void deviceState(boolean state) {
                if (state) {
                    txtDevState.setText(R.string.dev_state_connected);
                } else {
                    txtDevState.setText(R.string.dev_state_disconnected);
                    txtDevBatteryPower.setText(R.string.dev_power_empty);
                }
                updateContentFragment(state);
            }
        });
        showDemoMode();
    }

    private void updateDemoModeButton(boolean connectionState) {
        if (!connectionState) {
            if (_demoModeFragment == null || _demoMode != DemoMode.START)
                return;
            _demoModeFragment.updateButtonState();
        }
    }

    private void updateContentFragment(boolean connectionState) {
        updateDemoModeButton(connectionState);
        if (connectionState || _demoMode != DemoMode.DEV_SEARCH)
            showDemoMode();
    }

    private void stopProcess() {
        FragmentManager fm = getSupportFragmentManager();
        for (Fragment it : fm.getFragments()) {
            if (it.getClass().isAssignableFrom(ICallibriFragment.class)) {
                ((ICallibriFragment) it).stopProcess();
            }
        }
    }

    private void hideBackButton() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(null);
    }

    private void showBackButton() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void showDemoMode() {
        stopProcess();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (_demoModeFragment == null) {
            _demoModeFragment = DemoModeFragment.newInstance();
            _demoModeFragment.setCallback(new DemoModeFragment.IDemoModeCallback() {
                @Override
                public void modeDevSearch() {
                    showDevSearch();
                }

                @Override
                public void modeDevInfo() {
                    showDevInfo();
                }

                @Override
                public void modeSignalDemo() {
                    showSignalDemoMode();
                }

                @Override
                public void modeEnvelopeDemo() {
                    showEnvelopeDemoMode();
                }

                @Override
                public void modeEcgDemo() {
                    showEcgDemoMode();
                }

                @Override
                public void modeEEGDemo() {
                    showEEGDemoMode();
                }

                @Override
                public void modeRespirationDemo() {
                    showRespirationDemoMode();
                }

                @Override
                public void modeStimulationDemo() {
                    showStimulationDemoMode();
                }

                @Override
                public void modeSpectrumDemo() {
                    showSpectrumDemoMode();
                }

                @Override
                public void modeSpectrumPowerDemo() {
                    showSpectrumPowerDemoMode();
                }
            });
        }
        ft.replace(R.id.container, _demoModeFragment);
        ft.commit();
        _demoMode = DemoMode.START;
        hideBackButton();
    }

    private void showDevSearch() {
        stopProcess();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (_deDevSearchFragment == null) {
            _deDevSearchFragment = DevSearchFragment.newInstance();
        }
        ft.replace(R.id.container, _deDevSearchFragment);
        ft.commit();
        _demoMode = DemoMode.DEV_SEARCH;
        showBackButton();
    }

    private void showDevInfo() {
        stopProcess();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (_deviceInfoFragment == null) {
            _deviceInfoFragment = DeviceInfoFragment.newInstance();
        }
        ft.replace(R.id.container, _deviceInfoFragment);
        ft.commit();
        _demoMode = DemoMode.DEV_INF;
        showBackButton();
    }

    private void showSignalDemoMode() {
        stopProcess();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (_signalDemoFragment == null) {
            _signalDemoFragment = SignalDemoFragment.newInstance();
        }
        ft.replace(R.id.container, _signalDemoFragment);
        ft.commit();
        _demoMode = DemoMode.SIGNAL_MODE;
        showBackButton();
    }

    private void showEnvelopeDemoMode() {
        stopProcess();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (_envelopeDemoFragment == null) {
            _envelopeDemoFragment = EnvelopeDemoFragment.newInstance();
        }
        ft.replace(R.id.container, _envelopeDemoFragment);
        ft.commit();
        _demoMode = DemoMode.ENVELOPE_MODE;
        showBackButton();
    }

    private void showEcgDemoMode() {
        stopProcess();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (_ecgDemoFragment == null) {
            _ecgDemoFragment = EcgDemoFragment.newInstance();
        }
        ft.replace(R.id.container, _ecgDemoFragment);
        ft.commit();
        _demoMode = DemoMode.ECG_MODE;
        showBackButton();
    }

    private void showEEGDemoMode() {
        stopProcess();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (_eegDemoFragment == null) {
            _eegDemoFragment = EEGDemoFragment.newInstance();
        }
        ft.replace(R.id.container, _eegDemoFragment);
        ft.commit();
        _demoMode = DemoMode.EEG_MODE;
        showBackButton();
    }

    private void showRespirationDemoMode() {
        stopProcess();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (_respirationDemoFragment == null) {
            _respirationDemoFragment = RespirationDemoFragment.newInstance();
        }
        ft.replace(R.id.container, _respirationDemoFragment);
        ft.commit();
        _demoMode = DemoMode.RESPIRATION_MODE;
        showBackButton();
    }

    private void showStimulationDemoMode(){
        stopProcess();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (_stimulationDemoFragment == null) {
            _stimulationDemoFragment = StimulationDemoFragment.newInstance();
        }
        ft.replace(R.id.container, _stimulationDemoFragment);
        ft.commit();
        _demoMode = DemoMode.STIMULATION_MODE;
        showBackButton();
    }

    private void showSpectrumDemoMode() {
        stopProcess();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (_spectrumDemoFragment == null) {
            _spectrumDemoFragment = SpectrumDemoFragment.newInstance();
        }
        ft.replace(R.id.container, _spectrumDemoFragment);
        ft.commit();
        _demoMode = DemoMode.SPECTRUM_MODE;
        showBackButton();
    }

    private void showSpectrumPowerDemoMode() {
        stopProcess();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (_spectrumPowerDemoFragment == null) {
            _spectrumPowerDemoFragment = SpectrumPowerDemoFragment.newInstance();
        }
        ft.replace(R.id.container, _spectrumPowerDemoFragment);
        ft.commit();
        _demoMode = DemoMode.SPECTRUM_POWER_MODE;
        showBackButton();
    }

    @Override
    public void onBackPressed() {
        if (_demoMode == DemoMode.START)
            super.onBackPressed();
        else {
            showDemoMode();
        }
    }

    private enum DemoMode {
        START,
        DEV_SEARCH,
        DEV_INF,
        SIGNAL_MODE,
        ENVELOPE_MODE,
        ECG_MODE,
        EEG_MODE,
        RESPIRATION_MODE,
        STIMULATION_MODE,
        SPECTRUM_MODE,
        SPECTRUM_POWER_MODE,
    }
}
