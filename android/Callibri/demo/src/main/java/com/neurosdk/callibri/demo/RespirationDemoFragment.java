package com.neurosdk.callibri.demo;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.androidplot.xy.XYPlot;
import com.neuromd.neurosdk.ADCInput;
import com.neuromd.neurosdk.ChannelInfo;
import com.neuromd.neurosdk.ChannelType;
import com.neuromd.neurosdk.Command;
import com.neuromd.neurosdk.Device;
import com.neuromd.neurosdk.DeviceState;
import com.neuromd.neurosdk.ExternalSwitchInput;
import com.neuromd.neurosdk.Gain;
import com.neuromd.neurosdk.INotificationCallback;
import com.neuromd.neurosdk.Parameter;
import com.neuromd.neurosdk.ParameterName;
import com.neuromd.neurosdk.RespirationChannel;
import com.neuromd.neurosdk.SamplingFrequency;
import com.neuromd.neurosdk.SignalChannel;
import com.neurosdk.callibri.example.utils.CommonHelper;
import com.neurosdk.callibri.example.utils.PlotHolder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RespirationDemoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RespirationDemoFragment extends Fragment implements ICallibriFragment {
    private final String TAG = "[RespirationDemo]";
    private PlotHolder plot;

    public RespirationDemoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment
     *
     * @return A new instance of fragment DemoModeFragment.
     */
    public static RespirationDemoFragment newInstance() {
        return new RespirationDemoFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return initPlot(inflater.inflate(R.layout.fragment_respiration_demo, container, false));
    }

    private View initPlot(View rootView) {
        plot = new PlotHolder((XYPlot) rootView.findViewById(R.id.plot_signal));
//        rootView.findViewById(R.id.btn_zoom_in).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                plot.zoomYIn();
//            }
//        });
//        rootView.findViewById(R.id.btn_zoom_out).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                plot.zoomYOut();
//            }
//        });
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            Device device = DevHolder.inst().device();
            if (device != null) {
                ChannelInfo channelInfo = DevHolder.inst().getDevChannel(ChannelType.Respiration);
                if (channelInfo != null) {
                    configureDevice(device);
                    plot.startRender(new RespirationChannel(device), PlotHolder.ZoomVal.V_AUTO_1, 5.0f);
                }
            }
        } catch (Exception ex) {
            Log.d(TAG, "Failed start signal", ex);
            CommonHelper.showMessage(this, R.string.err_start_signal);
        }
    }

    private void configureDevice(Device device) {
        Parameter[] params = device.parameters();
        for (Parameter it : params) {
            switch (it.getName()) {
                case Gain:
                    if (device.readParam(ParameterName.Gain) != Gain.Gain6)
                        device.setParam(ParameterName.Gain, Gain.Gain6);
                    break;
                case Offset:
                    if ((byte) device.readParam(ParameterName.Offset) != 3)
                        device.setParam(ParameterName.Offset, (byte) 3);
                    break;
                case HardwareFilterState:
                    if (!(boolean) device.readParam(ParameterName.HardwareFilterState))
                        device.setParam(ParameterName.HardwareFilterState, true);
                    break;
                case ExternalSwitchState:
                    if (device.readParam(ParameterName.ExternalSwitchState) != ExternalSwitchInput.RespUSB)
                        device.setParam(ParameterName.ExternalSwitchState, ExternalSwitchInput.RespUSB);
                    break;
            }
        }
        device.execute(Command.StartRespiration);
    }

    @Override
    public void stopProcess() {
        if (plot != null) {
            plot.stopRender();
            try {
                Device device = DevHolder.inst().device();
                if (device != null) {
                    if (device.readParam(ParameterName.State) == DeviceState.Connected) {
                        device.execute(Command.StopRespiration);
                    }
                }
            } catch (Exception ex) {
                Log.d(TAG, "Failed stop signal", ex);
                CommonHelper.showMessage(this, R.string.err_stop_signal);
            }
        }
    }
}