package com.neurosdk.callibri.demo;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.androidplot.xy.XYPlot;
import com.neuromd.neurosdk.ADCInput;
import com.neuromd.neurosdk.ChannelInfo;
import com.neuromd.neurosdk.ChannelType;
import com.neuromd.neurosdk.Command;
import com.neuromd.neurosdk.Device;
import com.neuromd.neurosdk.DeviceState;
import com.neuromd.neurosdk.EcgChannel;
import com.neuromd.neurosdk.ElectrodeState;
import com.neuromd.neurosdk.ElectrodesStateChannel;
import com.neuromd.neurosdk.ExternalSwitchInput;
import com.neuromd.neurosdk.Gain;
import com.neuromd.neurosdk.HeartRateChannel;
import com.neuromd.neurosdk.INotificationCallback;
import com.neuromd.neurosdk.Parameter;
import com.neuromd.neurosdk.ParameterName;
import com.neuromd.neurosdk.RPeakChannel;
import com.neuromd.neurosdk.SamplingFrequency;
import com.neuromd.neurosdk.SignalChannel;
import com.neuromd.neurosdk.StressIndexChannel;
import com.neurosdk.callibri.example.utils.CommonHelper;
import com.neurosdk.callibri.example.utils.PlotHolder;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EcgDemoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EcgDemoFragment extends Fragment implements ICallibriFragment {
    private final String TAG = "[SignalDemo]";
    private PlotHolder plot;
    private final AtomicInteger _hr = new AtomicInteger();
    private final AtomicReference<Double> _si = new AtomicReference<>(0.0);
    private final AtomicBoolean _elState = new AtomicBoolean();

    private ElectrodesStateChannel _elStChannel;
    private HeartRateChannel _hrChannel;
    private StressIndexChannel _siChannel;
    private Future<?> _futureUpd;

    public EcgDemoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment
     *
     * @return A new instance of fragment DemoModeFragment.
     */
    public static EcgDemoFragment newInstance() {
        return new EcgDemoFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return initPlot(inflater.inflate(R.layout.fragment_ecg_demo, container, false));
    }

    private View initPlot(View rootView) {
        plot = new PlotHolder((XYPlot) rootView.findViewById(R.id.plot_signal));
        rootView.findViewById(R.id.btn_zoom_in).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plot.zoomYIn();
            }
        });
        rootView.findViewById(R.id.btn_zoom_out).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plot.zoomYOut();
            }
        });
        return rootView;
    }

    private void updateData() {
        FragmentActivity activity = getActivity();
        if (activity != null && isAdded()) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    View view = getView();
                    if (view != null && isAdded()) {
                        TextView txtHRValue = view.findViewById(R.id.txt_hr_value);
                        TextView txtSIValue = view.findViewById(R.id.txt_si_value);
                        TextView txtElStValue = view.findViewById(R.id.txt_el_state_value);
                        if (txtHRValue != null)
                            txtHRValue.setText(String.valueOf(_hr.get()));
                        if (txtSIValue != null) {
                            long si = Math.round(_si.get());
                            txtSIValue.setText(si > 0 ? String.valueOf(si) : getString(R.string.stress_index_zero));
                        }
                        if (txtElStValue != null)
                            txtElStValue.setText(_elState.get() ? R.string.el_state_normal : R.string.el_state_detached);
                    }
                }
            });
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            Device device = DevHolder.inst().device();
            if (device != null) {
                ChannelInfo channelInfo = DevHolder.inst().getDevChannel(ChannelType.Signal);
                if (channelInfo != null) {
                    configureDevice(device);
                    initChannels(device, channelInfo);

                    _futureUpd = Executors.newFixedThreadPool(1).submit(new Runnable() {
                        @SuppressWarnings("BusyWait")
                        @Override
                        public void run() {
                            try {
                                while (!Thread.currentThread().isInterrupted()) {
                                    Thread.sleep(500);
                                    updateData();
                                }
                            } catch (Exception ignored) {
                            }
                        }
                    });
                }
            }
        } catch (Exception ex) {
            Log.d(TAG, "Failed start signal", ex);
            CommonHelper.showMessage(this, R.string.err_start_signal);
        }
    }

    private void initChannels(Device device, ChannelInfo channelInfo) {
        // Create all channel
        EcgChannel ecgChannel = new EcgChannel(device, channelInfo);
        plot.startRender(ecgChannel, PlotHolder.ZoomVal.V_0002, 5.0f);

        _elStChannel = new ElectrodesStateChannel(device);
        SignalChannel signalChannel = new SignalChannel(device, channelInfo);
        final RPeakChannel rPeakChannel = new RPeakChannel(signalChannel, _elStChannel);
        _hrChannel = new HeartRateChannel(rPeakChannel);
        _siChannel = new StressIndexChannel(rPeakChannel); // Determining the stress index can take several minutes

        _hrChannel.dataLengthChanged.subscribe(new INotificationCallback<Integer>() {
            @Override
            public void onNotify(Object o, Integer integer) {
                int ttLen = _hrChannel.totalLength();
                if (ttLen > 0) {
                    int[] hrVals = _hrChannel.readData(ttLen - 1, 1);
                    if (hrVals != null && hrVals.length > 0)
                        _hr.lazySet(hrVals[0]);
                }
            }
        });
        _siChannel.dataLengthChanged.subscribe(new INotificationCallback<Integer>() {
            @Override
            public void onNotify(Object o, Integer integer) {
                int ttLen = _siChannel.totalLength();
                if (ttLen > 0) {
                    double[] siVals = _siChannel.readData(ttLen - 1, 1);
                    if (siVals != null && siVals.length > 0)
                        _si.lazySet(siVals[0]);
                }
            }
        });
        _elStChannel.dataLengthChanged.subscribe(new INotificationCallback<Integer>() {
            @Override
            public void onNotify(Object o, Integer integer) {
                int ttLen = _elStChannel.totalLength();
                if (ttLen > 0) {
                    ElectrodeState[] st = _elStChannel.readData(ttLen - 1, 1);
                    if (st != null && st.length > 0)
                        _elState.lazySet(st[0] == ElectrodeState.Normal);
                }
            }
        });
    }

    private void configureDevice(Device device) {
        Parameter[] params = device.parameters();
        for (Parameter it : params) {
            switch (it.getName()) {
                case Gain:
                    if (device.readParam(ParameterName.Gain) != Gain.Gain6)
                        device.setParam(ParameterName.Gain, Gain.Gain6);
                    break;
                case Offset:
                    if ((byte) device.readParam(ParameterName.Offset) != 3)
                        device.setParam(ParameterName.Offset, (byte) 3);
                    break;
                case ADCInputState:
                    if (device.readParam(ParameterName.ADCInputState) != ADCInput.Resistance)
                        device.setParam(ParameterName.ADCInputState, ADCInput.Resistance);
                    break;
                case SamplingFrequency:
                    if (device.readParam(ParameterName.SamplingFrequency) != SamplingFrequency.Hz125)
                        device.setParam(ParameterName.SamplingFrequency, SamplingFrequency.Hz125);
                    break;
                case HardwareFilterState:
                    if (!(boolean) device.readParam(ParameterName.HardwareFilterState))
                        device.setParam(ParameterName.HardwareFilterState, true);
                    break;
                case ExternalSwitchState:
                    if (device.readParam(ParameterName.ExternalSwitchState) != ExternalSwitchInput.MioElectrodes)
                        device.setParam(ParameterName.ExternalSwitchState, ExternalSwitchInput.MioElectrodes);
                    break;
            }
        }
        device.execute(Command.StartSignal);
    }

    @Override
    public void stopProcess() {
        if (plot != null) {
            plot.stopRender();
            try {
                if (_elStChannel != null) {
                    _elStChannel.dataLengthChanged.unsubscribe();
                    _elStChannel = null;
                }
                if (_hrChannel != null) {
                    _hrChannel.dataLengthChanged.unsubscribe();
                    _hrChannel = null;
                }
                if (_siChannel != null) {
                    _siChannel.dataLengthChanged.unsubscribe();
                    _siChannel = null;
                }
                Future<?> futureUpd = _futureUpd;
                if (futureUpd != null) {
                    _futureUpd = null;
                    futureUpd.cancel(true);
                }
                Device device = DevHolder.inst().device();
                if (device != null) {
                    if (device.readParam(ParameterName.State) == DeviceState.Connected) {
                        device.execute(Command.StopSignal);
                    }
                }
            } catch (Exception ex) {
                Log.d(TAG, "Failed stop signal", ex);
                CommonHelper.showMessage(this, R.string.err_stop_signal);
            }
        }
    }
}