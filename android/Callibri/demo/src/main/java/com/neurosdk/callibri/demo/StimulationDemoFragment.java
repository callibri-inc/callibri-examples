package com.neurosdk.callibri.demo;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.neuromd.neurosdk.Command;
import com.neuromd.neurosdk.Device;
import com.neuromd.neurosdk.DeviceState;
import com.neuromd.neurosdk.ParameterName;
import com.neuromd.neurosdk.StimulationParams;
import com.neurosdk.callibri.example.utils.CommonHelper;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link StimulationDemoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StimulationDemoFragment extends Fragment implements ICallibriFragment {
    private final String TAG = "[StimulationDemo]";

    private EditText amplitudeText;
    private EditText pulseText;
    private EditText frequencyText;
    private EditText durationText;

    private Button writeParam;
    private Button readParam;

    private Button startStimulation;
    private Button stopStimulation;

    public StimulationDemoFragment() {
        // Required empty public constructor
    }

    public static StimulationDemoFragment newInstance() {
        return new StimulationDemoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stimulation_demo, container, false);

        amplitudeText = view.findViewById(R.id.edit_amplitude);
        amplitudeText.addTextChangedListener(paramsChanged);

        pulseText = view.findViewById(R.id.edit_pulse);
        pulseText.addTextChangedListener(paramsChanged);

        frequencyText = view.findViewById(R.id.edit_frequency);
        frequencyText.addTextChangedListener(paramsChanged);

        durationText = view.findViewById(R.id.edit_duration);
        durationText.addTextChangedListener(paramsChanged);

        writeParam = view.findViewById(R.id.write_params);
        writeParam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeStimulationParam();
            }
        });

        readParam = view.findViewById(R.id.read_params);
        readParam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                readStimulationParam();
            }
        });

        startStimulation = view.findViewById(R.id.start_stimul);
        startStimulation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startStimulation.setEnabled(false);
                stopStimulation.setEnabled(true);
                startStimulation();
            }
        });

        stopStimulation = view.findViewById(R.id.stop_stimul);
        stopStimulation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startStimulation.setEnabled(true);
                stopStimulation.setEnabled(false);
                stopStimulation();
            }
        });
        return view;
    }

    public void writeStimulationParam() {
        try{
            writeStimulationParamPack(Integer.parseInt(amplitudeText.getText().toString()),
                    Integer.parseInt(pulseText.getText().toString()),
                    Integer.parseInt(frequencyText.getText().toString()),
                    Integer.parseInt(durationText.getText().toString()));

            amplitudeText.setText("");
            pulseText.setText("");
            frequencyText.setText("");
            durationText.setText("");
        }
        catch (Exception ex){
            Log.d(TAG, "Failed write params", ex);
        }

    }

    private void writeStimulationParamPack(int amplitude, int pulseWidth, int frequency, int stimulDuration ) {
        StimulationParams sp = new StimulationParams(amplitude, pulseWidth, frequency, stimulDuration);

        try{
            Device device = DevHolder.inst().device();
            if(device != null) {
                device.setParam(ParameterName.StimulatorParamPack, sp);
            }
        }
        catch(Exception ex){
            Log.d(TAG, "Failed stop signal", ex);
            CommonHelper.showMessage(this, ex.getMessage());
        }


    }

    public void readStimulationParam() {
        StimulationParams sp = readStimulationParamPack();

        amplitudeText.setText(String.format("%d",sp.amplitude()));
        pulseText.setText(String.format("%d",sp.pulseWidth()));
        frequencyText.setText(String.format("%d",sp.frequency()));
        durationText.setText(String.format("%d",sp.stimulusDuration()));
    }

    private StimulationParams readStimulationParamPack() {
        Device device = DevHolder.inst().device();
        if(device != null) {
            return device.readParam(ParameterName.StimulatorParamPack);
        }
        return new StimulationParams(0,0,0,0);
    }

    public void startStimulation() {
        try {
            Device device = DevHolder.inst().device();
            if (device != null) {
                if (device.readParam(ParameterName.State) == DeviceState.Connected) {
                    device.execute(Command.StartStimulation);
                }
            }
        } catch (Exception ex) {
            Log.d(TAG, "Failed stop signal", ex);
            CommonHelper.showMessage(this, R.string.err_start_stimulation);
        }
    }

    public void stopStimulation() {
        try {
            Device device = DevHolder.inst().device();
            if (device != null) {
                if (device.readParam(ParameterName.State) == DeviceState.Connected) {
                    device.execute(Command.StopStimulation);
                }
            }
        } catch (Exception ex) {
            Log.d(TAG, "Failed stop signal", ex);
            CommonHelper.showMessage(this, R.string.err_stop_stimulation);
        }
    }

    private TextWatcher paramsChanged = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            writeParam.setEnabled(amplitudeText.getText().length() > 0 &&
                    pulseText.getText().length() > 0 &&
                    frequencyText.getText().length() > 0 &&
                    durationText.getText().length() > 0);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    public void stopProcess() {
        stopStimulation();
    }
}