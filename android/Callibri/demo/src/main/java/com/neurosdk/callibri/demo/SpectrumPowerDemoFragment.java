package com.neurosdk.callibri.demo;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.neuromd.neurosdk.ADCInput;
import com.neuromd.neurosdk.ChannelInfo;
import com.neuromd.neurosdk.ChannelType;
import com.neuromd.neurosdk.Command;
import com.neuromd.neurosdk.Device;
import com.neuromd.neurosdk.DeviceState;
import com.neuromd.neurosdk.EegChannel;
import com.neuromd.neurosdk.ExternalSwitchInput;
import com.neuromd.neurosdk.Gain;
import com.neuromd.neurosdk.INotificationCallback;
import com.neuromd.neurosdk.Parameter;
import com.neuromd.neurosdk.ParameterName;
import com.neuromd.neurosdk.SamplingFrequency;
import com.neuromd.neurosdk.SpectrumChannel;
import com.neuromd.neurosdk.SpectrumPowerChannel;
import com.neurosdk.callibri.example.utils.CommonHelper;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReference;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link com.neurosdk.callibri.demo.SpectrumPowerDemoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SpectrumPowerDemoFragment extends Fragment implements ICallibriFragment {
    private final String TAG = "[SpectrumPowerDemo]";

    private Future<?> _futureUpd;

    private SpectrumPowerChannel _sppChannel;

    private final AtomicReference<Double> _sppVal = new AtomicReference<>(0.0);

    public SpectrumPowerDemoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment
     *
     * @return A new instance of fragment DemoModeFragment.
     */
    public static com.neurosdk.callibri.demo.SpectrumPowerDemoFragment newInstance() {
        return new com.neurosdk.callibri.demo.SpectrumPowerDemoFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_spectrum_power_demo, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            Device device = DevHolder.inst().device();
            if (device != null) {
                ChannelInfo channelInfoSignal = DevHolder.inst().getDevChannel(ChannelType.Signal);
                if (channelInfoSignal != null) {
                    configureDevice(device);
                    initChannels(device, channelInfoSignal);
                    _futureUpd = Executors.newFixedThreadPool(1).submit(new Runnable() {
                        @SuppressWarnings("BusyWait")
                        @Override
                        public void run() {
                            try {
                                while (!Thread.currentThread().isInterrupted()) {
                                    Thread.sleep(500);
                                    updateData();
                                }
                            } catch (Exception ignored) {
                            }
                        }
                    });
                }
            }
        } catch (Exception ex) {
            Log.d(TAG, "Failed start signal", ex);
            CommonHelper.showMessage(this, R.string.err_start_signal);
        }
    }

    /**
     * Update value in view
     *
     * @param txtValue display text field
     * @param sppVal   spectrum power value
     */
    private void updateViewValue(TextView txtValue, double sppVal) {
        if (txtValue != null) {
            txtValue.setText(getString(R.string.spectrum_power_value, sppVal * 1000.0));
        }
    }

    private void updateData() {
        FragmentActivity activity = getActivity();
        if (activity != null && isAdded()) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    View view = getView();
                    if (view != null && isAdded()) {
                        updateViewValue((TextView) view.findViewById(R.id.txt_spp_value), _sppVal.get());
                    }
                }
            });
        }
    }

    private void updateSPPValue(SpectrumPowerChannel resCh, AtomicReference<Double> sppVal) {
        int tt = resCh.totalLength();
        if (tt > 0) {
            double[] sppVals = resCh.readData(tt - 1, 1);
            if (sppVals != null && sppVals.length > 0) {
                sppVal.set(sppVals[0]);
            }
        }
    }

    private void initChannels(Device device, ChannelInfo channelInfoSignal) {
        // Alpha rhythm frequency 8-13 Hz
        _sppChannel = new SpectrumPowerChannel(new SpectrumChannel[]{new SpectrumChannel(new EegChannel(device, channelInfoSignal))}, 8, 13, "SPP_8_13Hz");

        _sppChannel.dataLengthChanged.subscribe(new INotificationCallback<Integer>() {
            @Override
            public void onNotify(Object sender, Integer nParam) {
                updateSPPValue(_sppChannel, _sppVal);
            }
        });
    }

    private void configureDevice(Device device) {
        Parameter[] params = device.parameters();
        for (Parameter it : params) {
            switch (it.getName()) {
                case Gain:
                    if (device.readParam(ParameterName.Gain) != Gain.Gain6)
                        device.setParam(ParameterName.Gain, Gain.Gain6);
                    break;
                case Offset:
                    if ((byte) device.readParam(ParameterName.Offset) != 3)
                        device.setParam(ParameterName.Offset, (byte) 3);
                    break;
                case ADCInputState:
                    if (device.readParam(ParameterName.ADCInputState) != ADCInput.Resistance)
                        device.setParam(ParameterName.ADCInputState, ADCInput.Resistance);
                    break;
                case SamplingFrequency:
                    if (device.readParam(ParameterName.SamplingFrequency) != SamplingFrequency.Hz250)
                        device.setParam(ParameterName.SamplingFrequency, SamplingFrequency.Hz250);
                    break;
                case HardwareFilterState:
                    if (!(boolean) device.readParam(ParameterName.HardwareFilterState))
                        device.setParam(ParameterName.HardwareFilterState, true);
                    break;
                case ExternalSwitchState:
                    if (device.readParam(ParameterName.ExternalSwitchState) != ExternalSwitchInput.MioElectrodes)
                        device.setParam(ParameterName.ExternalSwitchState, ExternalSwitchInput.MioElectrodes);
                    break;
            }
        }
        device.execute(Command.StartSignal);
    }

    @Override
    public void stopProcess() {
        try {
            Device device = DevHolder.inst().device();
            if (device != null) {
                if (_sppChannel != null) {
                    _sppChannel.dataLengthChanged.unsubscribe();
                    _sppChannel = null;
                }
                Future<?> futureUpd = _futureUpd;
                if (futureUpd != null) {
                    _futureUpd = null;
                    futureUpd.cancel(true);
                }
                if (device.readParam(ParameterName.State) == DeviceState.Connected) {
                    device.execute(Command.StopSignal);
                }
            }
        } catch (Exception ex) {
            Log.d(TAG, "Failed stop signal", ex);
            CommonHelper.showMessage(this, R.string.err_stop_signal);
        }
    }
}