//
//  DeviceInfoViewController.swift
//  CallibriExample
//
//  Created by Vladimir on 25.02.2021.
//

import UIKit

class DeviceInfoViewController: UIViewController {

    @IBOutlet weak var deviceInfoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        deviceInfoLabel.text = CallibriManager.shared.getDeviceInfo()
    }
    

}
