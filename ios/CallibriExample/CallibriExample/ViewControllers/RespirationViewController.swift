//
//  RespirationViewController.swift
//  CallibriExample
//
//  Created by Vladimir on 18.03.2021.
//

import UIKit

class RespirationViewController: UIViewController {
    @IBOutlet weak var respirationView: MyGraphView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        respirationView.setChannelName(name: "Respiration")
        respirationView.initGraph()
        CallibriManager.shared.respirationDelegate = respirationView
        CallibriManager.shared.readRespirationData()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CallibriManager.shared.stopReadingRespirationData()
    }

}
