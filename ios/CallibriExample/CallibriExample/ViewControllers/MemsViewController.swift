//
//  MemsViewController.swift
//  CallibriExample
//
//  Created by Vladimir on 31.03.2021.
//

import UIKit
import neurosdk

class MemsViewController: UIViewController, MemsDataDelegate {

    @IBOutlet weak var axLabel: UILabel!
    @IBOutlet weak var ayLabel: UILabel!
    @IBOutlet weak var azLabel: UILabel!
    @IBOutlet weak var gxLabel: UILabel!
    @IBOutlet weak var gyLabel: UILabel!
    @IBOutlet weak var gzLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CallibriManager.shared.MemsDelegate = self
        CallibriManager.shared.readMemsData()
    }
    
    func dataChanged(newValue: NTMEMSValue) {
        axLabel.text = "X: " + String(format:"%.4f", newValue.accelerometer.X)
        ayLabel.text = "Y: " + String(format:"%.4f", newValue.accelerometer.Y)
        azLabel.text = "Z: " + String(format:"%.4f", newValue.accelerometer.Z)
        
        gxLabel.text = "X: " + String(format:"%.4f", newValue.gyroscope.X)
        gyLabel.text = "Y: " + String(format:"%.4f", newValue.gyroscope.Y)
        gzLabel.text = "Z: " + String(format:"%.4f", newValue.gyroscope.Z)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CallibriManager.shared.stopReadingMemsData()
    }

}
