//
//  DeviceSearchViewController.swift
//  CallibriExample
//
//  Created by Vladimir on 25.02.2021.
//

import UIKit

class DeviceSearchViewController: UIViewController {

    @IBOutlet weak var deviceLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func SearchAndConnectBtnAction(_ sender: Any) {
        CallibriManager.shared.searchAndConnect(deviceLabel: deviceLabel)
    }
}
