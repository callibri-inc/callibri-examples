//
//  OrientationViewController.swift
//  CallibriExample
//
//  Created by Vladimir on 30.03.2021.
//

import UIKit
import neurosdk

class OrientationViewController: UIViewController, OrientationDataDelegate {

    @IBOutlet weak var wLabel: UILabel!
    @IBOutlet weak var xLabel: UILabel!
    @IBOutlet weak var yLabel: UILabel!
    @IBOutlet weak var zLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CallibriManager.shared.orientationDelegate = self
        CallibriManager.shared.readOrientationData()
    }
    
    func dataChanged(newValue: NTOrientationValue) {
        wLabel.text = "W: " + String(format:"%.4f", newValue.w)
        xLabel.text = "X: " + String(format:"%.4f", newValue.x)
        yLabel.text = "Y: " + String(format:"%.4f", newValue.y)
        zLabel.text = "Z: " + String(format:"%.4f", newValue.z)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CallibriManager.shared.stopReadingOrientationData()
    }

}
