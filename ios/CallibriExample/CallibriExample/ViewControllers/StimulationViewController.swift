//
//  StimulationViewController.swift
//  CallibriExample
//
//  Created by Aseatari on 23.06.2021.
//

import UIKit

class StimulationViewController: UIViewController {
    @IBOutlet weak var startStimull: UIButton!
    @IBOutlet weak var stopStimul: UIButton!
    @IBOutlet weak var readStimul: UIButton!
    @IBOutlet weak var writeStimul: UIButton!
    
    @IBOutlet weak var ampValue: UITextField!
    @IBOutlet weak var pulseValue: UITextField!
    @IBOutlet weak var frequencyValue: UITextField!
    @IBOutlet weak var durationValue: UITextField!
    
    @IBAction func StartStimulAction(_ sender: Any) {
        stopStimul.isEnabled = true
        startStimull.isEnabled = false
        CallibriManager.shared.startStimulation()
    }
    
    @IBAction func StopStimulAction(_ sender: Any) {
        stopStimul.isEnabled = false
        startStimull.isEnabled = true
        CallibriManager.shared.stopStimulation()
    }
    
    @IBAction func ReadStimulAction(_ sender: Any) {
        let params = CallibriManager.shared.readStimulationParams()
        
        ampValue.text = "\(params.current)"
        pulseValue.text = "\(params.pulseWidth)"
        frequencyValue.text = "\(params.frequency)"
        durationValue.text = "\(params.stimulusDuration)"
    }
    
    @IBAction func WriteStimulAction(_ sender: Any) {
        let amp = Int(ampValue.text ?? "0") ?? 0
        let pulse = Int(pulseValue.text ?? "0") ?? 0
        let freq = Int(frequencyValue.text ?? "0") ?? 0
        let duration = Int(durationValue.text ?? "0") ?? 0
        
        CallibriManager.shared.writeStimulationParams(amplitude: Int32(amp), pulse: Int32(pulse), frequency: Int32(freq), duration: Int32(duration))
        
        ampValue.text = ""
        pulseValue.text = ""
        frequencyValue.text = ""
        durationValue.text = ""
    }
    
    @IBAction func ampValueChanged(_ sender: Any) {
        paramChanged()
    }
    
    @IBAction func pulseValueChanged(_ sender: Any) {
        paramChanged()
    }
    
    @IBAction func frequencyValueChanged(_ sender: Any) {
        paramChanged()
    }
    
    @IBAction func durationValueChanged(_ sender: Any) {
        paramChanged()
    }
    
    private func paramChanged(){
        if(ampValue.text!.isEmpty || pulseValue.text!.isEmpty || frequencyValue.text!.isEmpty || durationValue.text!.isEmpty){
            writeStimul.isEnabled = false
        }
        else {
            writeStimul.isEnabled = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    CallibriManager.shared.stopStimulation()
  }
}
