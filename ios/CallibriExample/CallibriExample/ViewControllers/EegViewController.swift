//
//  EegViewController.swift
//  CallibriExample
//
//  Created by Vladimir on 26.03.2021.
//

import UIKit

class EegViewController: UIViewController {

    @IBOutlet weak var graphView: MyGraphView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        graphView.setChannelName(name: "EEG")
        graphView.initGraph()
        CallibriManager.shared.eegDelegate = graphView
        
        CallibriManager.shared.readEegData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CallibriManager.shared.stopReadingEegData()
    }

}
