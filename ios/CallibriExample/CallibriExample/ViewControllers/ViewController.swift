//
//  ViewController.swift
//  CallibriExample
//
//  Created by Vladimir on 24.02.2021.
//

import UIKit
import neurosdk

class ViewController: UIViewController, StateDelegate {

    @IBOutlet weak var SearchBtn: UIButton!
    @IBOutlet weak var InfoBtn: UIButton!
    @IBOutlet weak var SignalBtn: UIButton!
    @IBOutlet weak var HRBtn: UIButton!
    @IBOutlet weak var RespirationBtn: UIButton!
    @IBOutlet weak var SpectrumBtn: UIButton!
    @IBOutlet weak var EegBtn: UIButton!
    @IBOutlet weak var EnvelopeBtn: UIButton!
    @IBOutlet weak var OrientationBtn: UIButton!
    @IBOutlet weak var MemsBtn: UIButton!
    @IBOutlet weak var StimulationBtn: UIButton!
    
    @IBOutlet weak var connectionStateToolbarItem: UIBarButtonItem!
    @IBOutlet weak var batteryValueToolbarItem: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CallibriManager.shared.stateDelegate = self
    }
    
    func batteryValueDidChange(newValue: Int) {
        batteryValueToolbarItem.title = String(newValue) + "%"
    }
    
    func connectionStateDidChange(newState: NTState) {
        switch newState {
        case .connected:
            connectionStateToolbarItem.title = "Connected"
            break
        case .disconnected:
            connectionStateToolbarItem.title = "Disconnected"
            batteryValueToolbarItem.title = "-"
            break
        default:
            break
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(CallibriManager.shared.connectionState == .connected)
        {
            InfoBtn.isEnabled = true
            if(CallibriManager.shared.hasChannel(channelType: .signal))
            {
                SignalBtn.isEnabled = true
                HRBtn.isEnabled = true
                SpectrumBtn.isEnabled = true
                EegBtn.isEnabled = true
                OrientationBtn.isEnabled = true
            }
            else
            {
                SignalBtn.isEnabled = false
                HRBtn.isEnabled = false
                SpectrumBtn.isEnabled = false
                EegBtn.isEnabled = false
                OrientationBtn.isEnabled = false
            }
            RespirationBtn.isEnabled = CallibriManager.shared.hasChannel(channelType: .signal)
            MemsBtn.isEnabled = CallibriManager.shared.hasChannel(channelType: .MEMS)
            EnvelopeBtn.isEnabled = CallibriManager.shared.hasChannel(channelType: .envelope)
            StimulationBtn.isEnabled = CallibriManager.shared.hasParam(param: .stimulatorParamPack)
        }
        else
        {
            InfoBtn.isEnabled = false
            SignalBtn.isEnabled = false
            HRBtn.isEnabled = false
            SpectrumBtn.isEnabled = false
            EegBtn.isEnabled = false
            OrientationBtn.isEnabled = false
            RespirationBtn.isEnabled = false
            MemsBtn.isEnabled = false
            EnvelopeBtn.isEnabled = false
            StimulationBtn.isEnabled = false
        }
    }

    @IBAction func DeviceSearxhBtnAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let searchControlller = storyboard.instantiateViewController(identifier: "DeviceSearchViewController")
        navigationController?.pushViewController(searchControlller, animated: true)
    }
    @IBAction func DeviceInfoBtnAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let infoControlller = storyboard.instantiateViewController(identifier: "DeviceInfoViewController")
        navigationController?.pushViewController(infoControlller, animated: true)
    }
    
    @IBAction func EegBtnAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let eegControlller = storyboard.instantiateViewController(identifier: "SignalViewController")
        navigationController?.pushViewController(eegControlller, animated: true)
    }
    @IBAction func EcgHrBtnAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let hrControlller = storyboard.instantiateViewController(identifier: "EcgHrViewController")
        navigationController?.pushViewController(hrControlller, animated: true)
    }
    @IBAction func RespirationBtnAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let respirationController = storyboard.instantiateViewController(identifier: "RespirationViewController")
        navigationController?.pushViewController(respirationController, animated: true)
    }
    @IBAction func SpectrumBtnAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let spectrumController = storyboard.instantiateViewController(identifier: "SpectrumPowerViewController")
        navigationController?.pushViewController(spectrumController, animated: true)
    }
    @IBAction func TrueEegBtnAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let eegControlller = storyboard.instantiateViewController(identifier: "EegViewController")
        navigationController?.pushViewController(eegControlller, animated: true)
    }
    @IBAction func EnvelopeBtnAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let envelopeControlller = storyboard.instantiateViewController(identifier: "EnvelopeViewController")
        navigationController?.pushViewController(envelopeControlller, animated: true)
    }
    @IBAction func OrientationBtnAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let orientationControlller = storyboard.instantiateViewController(identifier: "OrientationViewController")
        navigationController?.pushViewController(orientationControlller, animated: true)
    }
    @IBAction func MemsBtnAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let memsControlller = storyboard.instantiateViewController(identifier: "MemsViewController")
        navigationController?.pushViewController(memsControlller, animated: true)
    }
    @IBAction func StimulationBtnAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let stimulationControlller = storyboard.instantiateViewController(identifier: "StimulationViewController")
        navigationController?.pushViewController(stimulationControlller, animated: true)
    }
    
}

