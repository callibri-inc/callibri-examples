//
//  EnvelopeViewController.swift
//  CallibriExample
//
//  Created by Vladimir on 26.03.2021.
//

import UIKit

class EnvelopeViewController: UIViewController {

    @IBOutlet weak var graphView: MyGraphView!
    @IBOutlet weak var notSupportedLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let version = CallibriManager.shared.getFirmwareVersion()
        if (version < 34) {
            notSupportedLabel.text = "Envelope channel is not supported \nfor this firmware version ( " + String(version) + " )"
            graphView.isHidden = true
        } else {
            notSupportedLabel.text = ""
            
            graphView.setChannelName(name: "Envelope")
            graphView.initGraph()
            CallibriManager.shared.envelopeDelegate = graphView
            CallibriManager.shared.readEnvelopeData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CallibriManager.shared.stopReadingEnvelopeData()
    }
    

}
