//
//  SignalViewController.swift
//  CallibriExample
//
//  Created by Vladimir on 01.03.2021.
//

import UIKit

class SignalViewController: UIViewController {

    @IBOutlet weak var SignalView: MyGraphView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SignalView.setChannelName(name: "Signal")
        SignalView.initGraph()
        CallibriManager.shared.signalDelegate = SignalView
        
        CallibriManager.shared.readSignalData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CallibriManager.shared.stopReadingSignalData()
    }

}
