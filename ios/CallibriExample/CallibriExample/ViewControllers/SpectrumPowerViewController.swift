//
//  SpectrumPowerViewController.swift
//  CallibriExample
//
//  Created by Vladimir on 25.03.2021.
//

import UIKit

class SpectrumPowerViewController: UIViewController {

    @IBOutlet weak var View1: MyGraphView!
    @IBOutlet weak var View2: MyGraphView!
    @IBOutlet weak var View3: MyGraphView!
    @IBOutlet weak var View4: MyGraphView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        View1.setChannelName(name: "delta")
        View1.initGraph()
        CallibriManager.shared.spectrumPowerDelegates.append(View1)
        
        View2.setChannelName(name: "theta")
        View2.initGraph()
        CallibriManager.shared.spectrumPowerDelegates.append(View2)
        
        View3.setChannelName(name: "alpha")
        View3.initGraph()
        CallibriManager.shared.spectrumPowerDelegates.append(View3)
        
        View4.setChannelName(name: "beta")
        View4.initGraph()
        CallibriManager.shared.spectrumPowerDelegates.append(View4)
        
        CallibriManager.shared.initEegChannels()
        CallibriManager.shared.readSpectrumChannels()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CallibriManager.shared.stopReadingSpectrumChannels()
    }
    

}
