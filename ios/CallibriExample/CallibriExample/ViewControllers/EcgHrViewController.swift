//
//  EcgHrViewController.swift
//  CallibriExample
//
//  Created by Vladimir on 03.03.2021.
//

import UIKit
import neurosdk

class EcgHrViewController: UIViewController, HeartRateDelegate {

    
    @IBOutlet weak var heartRateLabel: UILabel!
    @IBOutlet weak var stressIndexLabel: UILabel!
    @IBOutlet weak var electrodeStateLabel: UILabel!
    @IBOutlet weak var EcgView: MyGraphView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        heartRateLabel.text = "Heart Rate: "
        electrodeStateLabel.text = "Electrodes State: "
        stressIndexLabel.text = "Stress Index: "
        
        EcgView.setChannelName(name: "ECG")
        EcgView.initGraph()
        CallibriManager.shared.ecgDelegate = EcgView
        
        CallibriManager.shared.heartRateDelegate = self
        CallibriManager.shared.readHeartRateData()
    }
    
    func hrDataChanged(newValue: Int) {
        heartRateLabel.text = "Heart Rate: " + String(newValue)
    }
    
    func electrodeStateChanged(newValue: NTElectrodeState) {
        switch (newValue.type) {
        case .stateDetached:
            electrodeStateLabel.text = "Electrodes State: detached"
            break
        case .stateHighResistance:
            
            electrodeStateLabel.text = "Electrodes State: high resistance"
            break
        case .stateNormal:
            
            electrodeStateLabel.text = "Electrodes State: normal"
            break
        default:
            break
        }
    }
    
    func stressIndexChanged(newValue: Double) {
        stressIndexLabel.text = "Stress Index: " + String(newValue)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CallibriManager.shared.stopReadingHeartRateData()
        heartRateLabel.text = "Heart Rate: "
        electrodeStateLabel.text = "Electrodes State: "
        stressIndexLabel.text = "Stress Index: "
    }

}
