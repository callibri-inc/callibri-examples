//
//  CallibriManager.swift
//  CallibriExample
//
//  Created by Vladimir on 25.02.2021.
//

import UIKit
import neurosdk

protocol StateDelegate: AnyObject {
    func batteryValueDidChange(newValue:Int)
    func connectionStateDidChange(newState: NTState)
}

protocol SignalDataDelegate: AnyObject {
    func dataChanged(channelName: String, newValue: Double)
}

protocol HeartRateDelegate: AnyObject {
    func hrDataChanged(newValue: Int)
    func electrodeStateChanged(newValue: NTElectrodeState)
    func stressIndexChanged(newValue: Double)
}

protocol OrientationDataDelegate: AnyObject {
    func dataChanged(newValue: NTOrientationValue)
}

protocol MemsDataDelegate: AnyObject {
    func dataChanged(newValue: NTMEMSValue)
}

class CallibriManager: NSObject {

    static let shared = CallibriManager()
    
    private override init() {
        super.init()
    }
    
    private var scanner: NTDeviceEnumerator?
    private var device: NTDevice?
    private var deviceInfo: NTDeviceInfo?
    
    // channels
    private var batteryChannel: NTBatteryChannel? = nil
    private var signalChannels: [String: NTSignalChannel?] = [:]
    private var heartRateChannel: NTHeartRateChannel?
    private var RPeakChannel: NTRPeakChannel?
    private var electrodeStateChannel: NTElectrodeStateChannel?
    private var ecgChannel: NTEcgChannel?
    private var respirationChannel: NTRespirationChannel?
    private var eegChannel: NTEegChannel?
    private var spectrumPowerDoubleChannels: [String: NTSpectrumPowerDoubleChannel?] = [:]
    private var envelopeChannel: NTEnvelopeChannel?
    private var stressIndexChannel: NTStressIndexChannel?
    private var orientationChannel: NTOrientationChannel?
    private var MemsChannel: NTMEMSChannel?
    
    private var channelsInitialized: Bool = false
    
    // delegates
    weak var stateDelegate: StateDelegate?
    weak var signalDelegate: SignalDataDelegate?
    weak var heartRateDelegate: HeartRateDelegate?
    weak var ecgDelegate: SignalDataDelegate?
    weak var respirationDelegate: SignalDataDelegate?
    weak var eegDelegate: SignalDataDelegate?
    weak var envelopeDelegate: SignalDataDelegate?
    weak var orientationDelegate: OrientationDataDelegate?
    weak var MemsDelegate: MemsDataDelegate?
    var spectrumPowerDelegates: [SignalDataDelegate] = []
    
    public var batteryLevel: Int = 0 {
        didSet {
            stateDelegate?.batteryValueDidChange(newValue: batteryLevel)
        }
    }
    
    public var connectionState: NTState = .disconnected {
        didSet {
            stateDelegate?.connectionStateDidChange(newState: connectionState)
        }
    }
    
    func getFirmwareVersion() -> UInt {
        guard let device = self.device else { return 0 }
        return UInt(device.firmwareVersion(error: nil).version)
    }
    
    func hasChannel(channelType: NTChannelType) -> Bool{
        guard let device = self.device else { return false }
        do {
            try NTDeviceTraits.hasChannels(withType: device, channelType: channelType)
            return true
        }
        catch _ as NSError{
        }
        return false
    }
    
    func hasParam(param: NTParameter) -> Bool {
        guard let device = self.device else { return false }
        for p in device.parameters ?? [] {
            if(p.parameter == param)
            {
                return true
            }
        }
        return false
    }
    
    func searchAndConnect(deviceLabel: UILabel) {
        scanner = NTDeviceEnumerator(deviceType: .TypeCallibri)
        
        if let scanner = scanner {
            scanner.subscribeFoundDevice{ deviceInfo in
                self.device = NTDevice(enumerator: scanner, deviceInfo: deviceInfo)
                DispatchQueue.main.async {
                    deviceLabel.text = deviceInfo.name + " " + String(deviceInfo.serialNumber)
                }
                self.deviceInfo = deviceInfo
                
                if let d = self.device {
                    d.subscribeParameterChanged(subscriber: { param in
                        switch param {
                        case .state:
                            let state = d.state(error: nil)
                            self.connectionState = state
                            break

                            
                        default:
                            break
                        }
                    })
                }
                
                self.device?.connect()
                
                self.initChannels()
            }
            
        }
    }
    
    func initChannels() {
        if channelsInitialized {
            return
        }
        print("Initialization channels")
        initBattery()
        initSignalChannels()
        initHeartRateChannels()
        initRespiration()
        initEegChannels()
        initEnvelopeChannel()
        initOrientationChannel()
        initMemsChannel()
        /*initResistanceChannels()
        
        initIndexChannel()
        initMeditationAnalyzerChannel()
        initEmotionsAnalyzer()
        initBipolarChannels()
        initSpectrumChannel(.hamming)
        initSpectrumPowerChannel()
        initArtifactChannel()
        */
        channelsInitialized = true
    }

    
    func initBattery() {
        guard let device = self.device else { return }
        self.batteryChannel = NTBatteryChannel(device: device)

        guard let channel = batteryChannel else { return }
        channel.subscribeLengthChanged(subscribe: { length in
            guard let level = channel.readData(offset: length - 1, length: 1).first else { return }
            self.batteryLevel = level.intValue
            
        })
    }
    
    func initEnvelopeChannel() {
        guard let device = self.device else { return }
        
        do {
            try NTDeviceTraits.hasChannels(withType: device, channelType: .envelope)
            self.envelopeChannel = NTEnvelopeChannel(device: device)
            print("initEnvelopeChannel succeeded")
        }
        catch let err as NSError{
            print("initEnvelopeChannel failed")
        }
    }
    
    func readEnvelopeData() {
        self.device?.setGain(.G6, error: nil)
        self.device?.setOffset(3, error: nil)
        self.device?.setADCInputState(.resistance, error: nil)
        self.device?.setSamplingFrequency(.hz125, error: nil)
        self.device?.setHardwareFilterState(true, error: nil)
        self.device?.setExternalSwitchInput(NTExternalSwitchInput.mioElectrodes, error: nil)
        
        self.device?.execute(command: .startEnvelope)
        
        self.envelopeChannel?.subscribeLengthChanged(subscribe: { (length) in
            var newdata = self.envelopeChannel?.readData(offset: length-1, length: 1).first
            newdata = NSNumber(value: newdata!.doubleValue * 1000000)
            DispatchQueue.main.async {
                self.envelopeDelegate?.dataChanged(channelName: "Envelope", newValue: newdata as! Double)
            }
        })
    }
    
    func stopReadingEnvelopeData() {
        self.envelopeChannel?.subscribeLengthChanged(subscribe: nil)
        self.envelopeDelegate = nil
        
        self.device?.execute(command: .stopEnvelope)
    }
    
    func initSignalChannels() {
        if(!signalChannels.isEmpty) { return }
        guard let device = self.device else { return }
        do {
            try NTDeviceTraits.hasChannels(withType: device, channelType: .signal)
            NTDeviceTraits.getChannelInfoArray(withType: device, channelType: .signal, error: nil).forEach {
                signalChannels[$0.name] = NTSignalChannel(device: device, channelInfo: $0)
            }
            print("initSignalChannels succeeded")
            
        }
        catch let err as NSError{
            print("initSignalChannels failed")
        }
    }
    
    func readSignalData() {
        self.device?.setGain(.G6, error: nil)
        self.device?.setOffset(3, error: nil)
        self.device?.setADCInputState(.resistance, error: nil)
        self.device?.setSamplingFrequency(.hz125, error: nil)
        self.device?.setHardwareFilterState(true, error: nil)
        self.device?.setExternalSwitchInput(.mioElectrodes, error: nil)
        
        self.device?.execute(command: .startSignal)
        
        self.signalChannels.forEach { (channelName, channel) in
            channel?.subscribeLengthChanged(subscribe: { (length) in
                var newdata = channel?.readData(offset: length-1, length: 1).first
                newdata = NSNumber(value: newdata!.doubleValue * 1000000)
                DispatchQueue.main.async {
                    self.signalDelegate?.dataChanged(channelName: channelName, newValue: newdata as! Double)
                        
                }
            })
        }
    }
    
    func stopReadingSignalData() {
        self.signalChannels.forEach{ (_, channel) in
            channel?.subscribeLengthChanged(subscribe: nil)
        }
        self.signalDelegate = nil
        
        self.device?.execute(command: .stopSignal)
    }
    
    func initHeartRateChannels() {
        guard let device = self.device else { return }
        self.electrodeStateChannel = NTElectrodeStateChannel(device: device)
        
        self.ecgChannel = NTEcgChannel(device: device)
        
        if (!self.signalChannels.isEmpty) {
            self.RPeakChannel = NTRPeakChannel(signalChannel: (self.signalChannels["Signal"]!!), electrodeStateChannel: self.electrodeStateChannel)
            self.heartRateChannel = NTHeartRateChannel(rPeakChannel: self.RPeakChannel)
            self.stressIndexChannel = NTStressIndexChannel(rPeakChannel: self.RPeakChannel)
        }
    }
    
    func readHeartRateData() {
        self.device?.setGain(.G6, error: nil)
        self.device?.setOffset(3, error: nil)
        self.device?.setADCInputState(.resistance, error: nil)
        self.device?.setSamplingFrequency(.hz125, error: nil)
        self.device?.setHardwareFilterState(true, error: nil)
        self.device?.setExternalSwitchInput(.mioElectrodes, error: nil)
        
        self.device?.execute(command: .startSignal)
        
        self.heartRateChannel?.subscribeLengthChanged(subscribe: { (length) in
            let newdata = self.heartRateChannel?.readData(offset: length-1, length: 1).first
                    DispatchQueue.main.async {
                        self.heartRateDelegate?.hrDataChanged(newValue: newdata as! Int)
                    }
            })
        
        self.electrodeStateChannel?.subscribeLengthChanged(subscribe: { (length) in
            guard let newdata = self.electrodeStateChannel?.readData(offset: length-1, length: 1).first else { return }
            DispatchQueue.main.async {
                self.heartRateDelegate?.electrodeStateChanged(newValue: newdata)
            }
        })
        
        self.stressIndexChannel?.subscribeLengthChanged(subscribe: { (length) in
            let newdata = self.stressIndexChannel?.readData(offset: length-1, length: 1).first
            DispatchQueue.main.async {
                self.heartRateDelegate?.stressIndexChanged(newValue: newdata as! Double)
            }
        })
        
        self.ecgChannel?.subscribeLengthChanged(subscribe: { (length) in
            var newdata = self.ecgChannel?.readData(offset: length-1, length: 1).first
            newdata = NSNumber(value: newdata!.doubleValue * 1000000)
            DispatchQueue.main.async {
                self.ecgDelegate?.dataChanged(channelName: "ECG", newValue: newdata as! Double)
                
            }
        })
    }
    
    func stopReadingHeartRateData() {
        self.heartRateChannel?.subscribeLengthChanged(subscribe: nil)
        self.electrodeStateChannel?.subscribeLengthChanged(subscribe: nil)
        self.ecgChannel?.subscribeLengthChanged(subscribe: nil)
        self.heartRateDelegate = nil
        self.ecgDelegate = nil
        
        self.device?.execute(command: .stopSignal)
    }
    
    func initRespiration() {
        guard let device = self.device else { return }
        do {
            try NTDeviceTraits.hasChannels(withType: device, channelType: .respiration)
            NTDeviceTraits.getChannelInfoArray(withType: device, channelType: .respiration, error: nil).forEach { // must be once
                self.respirationChannel = NTRespirationChannel(device: device, channelInfo: $0 )
            }
            print("initResistanceChannels succeeded")
        }
        catch let err as NSError{
            print("initResistanceChannels failed")
        }
        
    }
    
    func readRespirationData() {
        self.device?.setGain(.G6, error: nil)
        self.device?.setOffset(3, error: nil)
        self.device?.setHardwareFilterState(true, error: nil)
        self.device?.setExternalSwitchInput(.respUSB, error: nil)
        
        self.device?.execute(command: .startRespiration)
        
        self.respirationChannel?.subscribeLengthChanged(subscribe: { (length) in
            var newdata = self.respirationChannel?.readData(offset: length-1, length: 1).first
            newdata = NSNumber(value: (newdata!.doubleValue - 0.59/*baseline*/) * 100000)
                    DispatchQueue.main.async {
                        self.respirationDelegate?.dataChanged(channelName: "Respiration", newValue: newdata as! Double )
                    }
                })
    }
    
    func stopReadingRespirationData() {
        self.respirationChannel?.subscribeLengthChanged(subscribe: nil)
        self.respirationDelegate = nil
        
        self.device?.execute(command: .stopRespiration)
    }
    
    func initEegChannels() {
        guard let device = self.device else { return }
        do {
            try NTDeviceTraits.hasChannels(withType: device, channelType: .signal)
            NTDeviceTraits.getChannelInfoArray(withType: device, channelType: .signal, error: nil).forEach {
                eegChannel = NTEegChannel(device: device, channelInfo: $0)
            }
            let spectrumChannel = NTSpectrumChannel(channel: eegChannel, windowType: NTSpectrumWindow.hamming)
            spectrumPowerDoubleChannels["delta"] = NTSpectrumPowerDoubleChannel(channel: [spectrumChannel!], lowFreq: 0.5, highFreq: 4.0, duration: 8.0)
            spectrumPowerDoubleChannels["theta"] = NTSpectrumPowerDoubleChannel(channel: [spectrumChannel!], lowFreq: 4.0, highFreq: 8.0, duration: 8.0)
            spectrumPowerDoubleChannels["alpha"] = NTSpectrumPowerDoubleChannel(channel: [spectrumChannel!], lowFreq: 8.0, highFreq: 14.0, duration: 8.0)
            spectrumPowerDoubleChannels["beta"] = NTSpectrumPowerDoubleChannel(channel: [spectrumChannel!], lowFreq: 14.0, highFreq: 24.0, duration: 8.0)
            
            print("initEegChannels succeeded")
        }
        catch let err as NSError{
            print("initEegChannels failed")
        }
    }
    
    func readEegData() {
        self.device?.setGain(.G6, error: nil)
        self.device?.setOffset(3, error: nil)
        self.device?.setADCInputState(.resistance, error: nil)
        self.device?.setSamplingFrequency(.hz250, error: nil)
        self.device?.setHardwareFilterState(true, error: nil)
        self.device?.setExternalSwitchInput(.mioElectrodes, error: nil)
        
        self.device?.execute(command: .startSignal)
                
        self.eegChannel?.subscribeLengthChanged(subscribe: { (length) in
            var newdata = self.eegChannel?.readData(offset: length-1, length: 1).first
            newdata = NSNumber(value: newdata!.doubleValue * 1000000)
            DispatchQueue.main.async {
                self.eegDelegate?.dataChanged(channelName: "EEG", newValue: newdata as! Double)
            }
        })
    }
    
    func stopReadingEegData() {
        self.eegChannel?.subscribeLengthChanged(subscribe: nil)
        self.eegDelegate = nil
        
        self.device?.execute(command: .stopSignal)
    }
    
    func readSpectrumChannels() {
        self.device?.setGain(.G6, error: nil)
        self.device?.setOffset(3, error: nil)
        self.device?.setADCInputState(.resistance, error: nil)
        self.device?.setSamplingFrequency(.hz250, error: nil)
        self.device?.setHardwareFilterState(true, error: nil)
        self.device?.setExternalSwitchInput(.mioElectrodes, error: nil)
        
        self.device?.execute(command: .startSignal)
        
        self.device?.setExternalSwitchInput(NTExternalSwitchInput.mioElectrodes, error: nil)
        
        self.spectrumPowerDoubleChannels.forEach { (channelName, channel) in
            channel?.subscribeLengthChanged(subscribe: { (length) in
                var newdata = channel?.readData(offset: length-1, length: 1).first
                newdata = NSNumber(value: newdata!.doubleValue * 100000)
                DispatchQueue.main.async {
                    self.spectrumPowerDelegates.forEach { delegate in
                        delegate.dataChanged(channelName: channelName, newValue: newdata as! Double)
                    }
                }
            })
        }
    }
    
    func stopReadingSpectrumChannels() {
        self.spectrumPowerDoubleChannels.forEach{ (_, channel) in
            channel?.subscribeLengthChanged(subscribe: nil)
        }
        self.spectrumPowerDelegates.removeAll()
        
        self.device?.execute(command: .stopSignal)
    }
    
    func initOrientationChannel() {
        guard let device = self.device else { return }
        self.orientationChannel = NTOrientationChannel(device: device)
    }
    
    func readOrientationData() {
        self.device?.execute(command: .startAngle)
        
        self.orientationChannel?.subscribeLengthChanged(subscribe: { length in
            let newdata: NTOrientationValue = self.orientationChannel?.readData(offset: length-1, length: 1).first ?? NTOrientationValue(w: 0, x: 0, y: 0, z: 0)
            DispatchQueue.main.async {
                self.orientationDelegate?.dataChanged(newValue: newdata)
            }
        })
    }
    
    func stopReadingOrientationData() {
        self.orientationChannel?.subscribeLengthChanged(subscribe: nil)
        self.orientationDelegate = nil
        
        self.device?.execute(command: .stopAngle)
    }
    
    
    func initMemsChannel() {
        guard let device = self.device else { return }
        self.MemsChannel = NTMEMSChannel(device: device)
    }
    
    func readMemsData() {
        self.device?.execute(command: .startMEMS)
        
        self.MemsChannel?.subscribeLengthChanged(subscribe: { length in
            let newdata : NTMEMSValue = self.MemsChannel?.readData(offset: length-1, length: 1).first ?? NTMEMSValue(accelerometer: Point3D(X: 0, Y: 0, Z: 0), gyroscope: Point3D(X: 0, Y: 0, Z: 0))
            DispatchQueue.main.async {
                self.MemsDelegate?.dataChanged(newValue: newdata)
            }
        })
    }
    
    func stopReadingMemsData() {
        self.MemsChannel?.subscribeLengthChanged(subscribe: nil)
        self.MemsDelegate = nil
        
        self.device?.execute(command: .stopMEMS)
    }
    
    func readStimulationParams() -> NTStimulationParams {
        let _default = NTStimulationParams(current: 0,pulseWidth: 0,frequency: 0,stimulusDuration: 0)
        guard let device = self.device else { return _default }
        
        return device.stimulatorParamPack(error: nil)
    }
    
    func writeStimulationParams(amplitude: Int32, pulse: Int32, frequency: Int32, duration: Int32) {
        guard let device = self.device else { return }
        
        device.setStimulatorParamPack(NTStimulationParams(current: amplitude, pulseWidth: pulse, frequency: frequency, stimulusDuration: duration), error: nil)
    }
    
    func startStimulation() {
        guard let device = self.device else { return }
        device.execute(command: .startStimulation)
    }
    
    func stopStimulation() {
        guard let device = self.device else { return }
        device.execute(command: .stopStimulation)
    }
    
    func getDeviceInfo() -> String {
        var result : String = ""
        
        if let device = self.device {
            
            if let deviceInfo = self.deviceInfo {
                result = "Name: " + deviceInfo.name + " \n"
                result = result + "Serial Number: " + String(deviceInfo.serialNumber) + " \n"
                result = result + "Address: " + deviceInfo.address + " \n"
            }
            
            result = result + "Battery Level: " + String(self.batteryLevel) + "% \n"
        }
        return result
    }
}
